#include "../constants.h"
#include "HandComparer.h"
#include <iostream>
#include <strings.h> // bzero

int compareHands(const Hand& A,const Hand& B)
{
  HandInfo Ainfo, Binfo;

  // parse the info for each hand
  getHandInfo(A,Ainfo);
  getHandInfo(B,Binfo);

  // print them for debugging purposes
  //std::cout << A.toString() << std::endl;
  //printHandInfo(Ainfo);
  //std::cout << B.toString() << std::endl;
  //printHandInfo(Binfo);

  // the one with a higher type wins
  if(Ainfo.type > Binfo.type) return  1;
  if(Ainfo.type < Binfo.type) return -1;

  // if both hands are the same type, it gets complicated
  // since every hand has its own rules for comparing itself

  switch(Ainfo.type)
  {
    case FiveOfAKind:
      // five-of-a-kind hands are ranked by the quintuplet
      return compareRanks(Ainfo.quintuple, Binfo.quintuple);

    case StraightFlush:
      // straignt flushes are ranked by their high card
      return compareRanks(Ainfo.highCard, Binfo.highCard);

    case FourOfAKind:
      // four-of-a-kind hands are ranked by their quadruplet
      // then by their fifth card
      if(compareRanks(Ainfo.quadruple, Binfo.quadruple) != 0)
      {
        return compareRanks(Ainfo.quadruple, Binfo.quadruple);
      }
      return compareRanks(Ainfo.highCard, Binfo.highCard);

    case FullHouse:
      // full houses are ranked by their triplet, then by their pair
      if(compareRanks(Ainfo.triple, Binfo.triple) != 0)
      {
        return compareRanks(Ainfo.triple, Binfo.triple);
      }
      return compareRanks(Ainfo.pair[0], Binfo.pair[0]);

    case Flush:
      // flushes are ranked by their highest non-equal card
      return compareHighestCard(Ainfo,Binfo,false);

    case Straight:
      // straights are ranked by their high card
      return compareRanks(Ainfo.highCard, Binfo.highCard);

    case ThreeOfAKind:
      // three-of-a-kind hands are ranked by their pair, then by their
      // other two cards
      if(compareRanks(Ainfo.triple, Binfo.triple) != 0)
      {
        return compareRanks(Ainfo.triple, Binfo.triple);
      }
      return compareHighestCard(Ainfo,Binfo,true);

    case TwoPair:
      // two-pair hands are ranked by their higher pair, then their lower pair
      // then the high card

      // brackets because we're declaring temp variables inside a switch
      {
        char aMax = Ainfo.pair[(Ainfo.pair[0] > Ainfo.pair[1] ? 0 : 1)];
        char bMax = Binfo.pair[(Binfo.pair[0] > Binfo.pair[1] ? 0 : 1)];
        if(compareRanks(aMax,bMax) != 0)
        {
          return compareRanks(aMax,bMax);
        }

        char aMin = Ainfo.pair[(Ainfo.pair[0] > Ainfo.pair[1] ? 1 : 0)];
        char bMin = Binfo.pair[(Binfo.pair[0] > Binfo.pair[1] ? 1 : 0)];
        if(compareRanks(aMin,bMin) != 0)
        {
          return compareRanks(aMin,bMin);
        }

        return compareRanks(Ainfo.highCard, Binfo.highCard);
      }

    case OnePair:
      // one-pair hands are ranked by their pair, then the rest of the cards
      if(compareRanks(Ainfo.pair[0],Binfo.pair[0]) != 0)
      {
        return compareRanks(Ainfo.pair[0],Binfo.pair[0]);
      }

      return compareHighestCard(Ainfo,Binfo,true);

    default:
      return compareHighestCard(Ainfo,Binfo,true);
  }
}

int compareRanks(const char& a,const char& b)
{
  if(rankToInt(a) > rankToInt(b)) return  1;
  if(rankToInt(a) < rankToInt(b)) return -1;
  return 0;
}

int compareHighestCard(const HandInfo& A, const HandInfo& B, bool excludePairs)
{
  // search from highest card down
  for(int i=MAX_RANK-1; i>=0; i--)
  {
    if(excludePairs)
    {
      // if a card is in one but not the other, one hand wins
      if(A.count[i] == 1 && B.count[i] == 0) return  1;
      if(B.count[i] == 1 && A.count[i] == 0) return -1;
    }
    else
    {
      if(A.count[i] > B.count[i]) return  1;
      if(B.count[i] > A.count[i]) return -1;
    }
  }   

  return 0;
}

void getHandInfo(const Hand& h, HandInfo& hi)
{
  // zero it out
  bzero(&hi,sizeof(HandInfo));

  // get the count of each card
  for(int i=0; i<5; i++)
  {
    hi.count[rankToInt(h.card[i].rank)]++;
  }

  // get the ranks of the N-of-a-kinds
  int pairPos = 0;  // there might be more than one pair
  for(int i=0; i<MAX_RANK; i++)
  {
    switch(hi.count[i])
    {
      case 5: hi.quintuple = intToRank(i); break;
      case 4: hi.quadruple = intToRank(i); break;
      case 3: hi.triple = intToRank(i); break;
      case 2: hi.pair[pairPos++] = intToRank(i); break;
    }
  }

  // determine whether or not the hand is a flush
  hi.flush = true;
  for(int i=1; hi.flush && i<5; i++)
  {
    if(h.card[0].suit != h.card[i].suit)
    {
      hi.flush = false;
    }
  }

  // determine whether or not the hand is a straight
  int streak=0;
  for(int i=0; i <= MAX_RANK; i++)
  {
    // is shifted slightly because ace-low straights are a thing
    if(hi.count[(i==0 ? MAX_RANK-1 : i-1)] == 1)
    {
      streak++;
    }
    else
    {
      streak=0;
    }

    if(streak==5)
    {
      hi.straight = true;
    }
  }

  // get the high card
  if(hi.straight && hi.count[rankToInt('A')] == 1
                 && hi.count[rankToInt('2')] == 1)
  {
    // if it's an ace-low straight, the high card is the 5, not the ace
    hi.highCard = '5';
  }
  else
  {
    // otherwise it's the literal highest card (not in an N-of-a-kind)
    for(int i=MAX_RANK-1; i>=0; i--)
    {
      if(hi.count[i] == 1)
      {
        hi.highCard = intToRank(i);
        break;
      }
    }
  }

  // determine the type of hand
       if(hi.quintuple != 0)                 hi.type = FiveOfAKind;
  else if(hi.straight && hi.flush)           hi.type = StraightFlush;
  else if(hi.quadruple != 0)                 hi.type = FourOfAKind;
  else if(hi.pair[0] != 0 && hi.triple != 0) hi.type = FullHouse;
  else if(hi.flush)                          hi.type = Flush;
  else if(hi.straight)                       hi.type = Straight;
  else if(hi.triple != 0)                    hi.type = ThreeOfAKind;
  else if(hi.pair[1] != 0)                   hi.type = TwoPair;
  else if(hi.pair[0] != 0)                   hi.type = OnePair;
  else                                       hi.type = HighCard;
}

void printHandInfo(const HandInfo& hi)
{
  /*
  for(int i=0; i<MAX_RANK; i++)
  {
    std::cout << intToRank(i) << ": " << hi.count[i] << (i%7==6 ? "\n" : "   ");
  }
  */
  std::cout << "--- " << typeToString(hi.type) << " ---" << std::endl;
  if(hi.highCard != 0) std::cout << "high card: " << hi.highCard << std::endl;

  if(hi.straight) std::cout << "STRAIGHT ";
  if(hi.flush) std::cout << "FLUSH ";
  std::cout << std::endl;

  if(hi.quintuple != 0) std::cout<<"5-of-a-kind: "<< hi.quintuple << std::endl;
  if(hi.quadruple != 0) std::cout<<"4-of-a-kind: "<< hi.quadruple << std::endl;
  if(hi.triple != 0)    std::cout<<"3-of-a-kind: "<< hi.triple << std::endl;
  if(hi.pair[0] != 0)
    std::cout << "pairs: " << hi.pair[0] << " " << hi.pair[1] << std::endl;

  std::cout << std::endl;
}

int rankToInt(char r)
{
  switch(r)
  {
    case 'T': return 8;
    case 'J': return 9;
    case 'Q': return 10;
    case 'K': return 11;
    case 'A': return 12;
    default : return (int)(r-'2');
  }
}

char intToRank(int i)
{
  switch(i)
  {
    case 8: return 'T';
    case 9: return 'J';
    case 10: return 'Q';
    case 11: return 'K';
    case 12: return 'A';
    default : return (char)(i+'2');
  }
}

int cardToInt(const Card& c)
{
  return (4 * rankToInt(c.rank)) + (int)c.suit;
}

std::string typeToString(HandType t)
{
  switch(t)
  {
    case HighCard:      return "High Card";
    case OnePair:       return "One Pair";
    case TwoPair:       return "Two Pair";
    case ThreeOfAKind:  return "Three of a Kind";
    case Straight:      return "Straight";
    case Flush:         return "Flush";
    case FullHouse:     return "Full House";
    case FourOfAKind:   return "Four of a Kind";
    case StraightFlush: return "Straight Flush";
    case FiveOfAKind:   return "Five of a Kind";
    default:            return "NULL";
  }
}
