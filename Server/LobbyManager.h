#pragma once

#include <string>
#include <vector>
#include <pthread.h>

#include "Player.h"
#include "Lobby.h"

class LobbyManager
{
  public:
    LobbyManager(unsigned int,unsigned int,unsigned int);
    ~LobbyManager();

    void addConnection(int);

    static void* threadLauncher(void*); // because pthread doesn't
                                        // work with member variables
    void* runInThread(void*);
    void addPlayer(Player*);

    pthread_mutex_t mutex;

  private:
    std::vector<Player*> playerList;
    std::vector<Player*> returnPlayers;
    std::vector<int> playersConnecting;

    std::vector<bool> freeIds;

    std::vector<Lobby*> lobbyList;

    unsigned int maxNumLobbies;
    unsigned int maxPlayersPerLobby;
    unsigned int maxUsernameLength;

    void clientJoinLobby(Player*, unsigned int, std::string);
    void clientCreateLobby(Player*, std::string, std::string);

    void handleRequest(unsigned int, Player*, std::string);

    void sendStatusMessage(int=-1);
};
