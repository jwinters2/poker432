#pragma once

enum HandType {HighCard, OnePair, TwoPair, ThreeOfAKind, 
               Straight, Flush, FullHouse, FourOfAKind,
               StraightFlush, FiveOfAKind};

struct HandInfo
{
  unsigned short count[MAX_RANK]; // the counts of each card
  char pair[2];   // the ranks of cards that come in pairs (2 are possible)
  char triple;    // the rank of the 3-of-a-kind (if there is one)
  char quadruple; // the rank of the 4-of-a-kind (if there is one)
  char quintuple; // the rank of the 5-of-a-kind (if there is one)
  bool flush;     // whether or not the hand is a flush (all same suit)
  bool straight;  // whether or not the hand is a straight
                  // (ranks form a sequence, e.g. 3 4 5 6 7)
  char highCard;  // the highest card that isn't part of an N-of-a-kind
  HandType type;  // the type of hand
};

int rankToInt(char);
char intToRank(int);
int cardToInt(const Card&);
void getHandInfo(const Hand&, HandInfo&);
void printHandInfo(const HandInfo&);

int compareHands(const Hand&,const Hand&);
int compareRanks(const char&,const char&);
int compareHighestCard(const HandInfo&, const HandInfo&, bool);

std::string typeToString(HandType);

