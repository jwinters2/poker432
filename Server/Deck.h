#pragma once

#include "../constants.h"

class Deck
{
  public:
    Deck(int);
    int drawCard();
    void reset();

    bool isEmpty() const;

    bool dealHand(Hand&);

  private:
    unsigned int cards[52];
    unsigned int cardCount;

    const int numDecks;
};
