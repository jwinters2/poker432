#include "Player.h"
#include "Global.h"
#include "../constants.h"

#include <iostream>
#include <string>     // std::string
#include <unistd.h>   // read(), write() for file descriptors
#include <cstring>    // memcmp()

Player::Player(int _sd, std::string _name):mutex(PTHREAD_MUTEX_INITIALIZER)
                                          ,name(_name),sd(_sd)
{
  pthread_t thread;
  std::cout << " - " << name << " starting thread" << std::endl;
  pthread_create(&thread, NULL, threadStarter, this);
}

Player::~Player()
{
  pthread_mutex_lock(&coutMutex);
  std::cout << "closing socket for player " << name << std::endl << std::flush;
  pthread_mutex_unlock(&coutMutex);
  close(sd);
}

bool Player::pollRequest() // the only reason this is const is that
                           // we're locking the mutex
{
  pthread_mutex_lock(&mutex);

  bool retval = (requestQueue.size() > 0);

  pthread_mutex_unlock(&mutex);

  return retval;
}

std::string Player::dequeueRequest()
{
  pthread_mutex_lock(&mutex);

  std::string retval = requestQueue.front();
  requestQueue.erase(requestQueue.begin());

  pthread_mutex_unlock(&mutex);

  return retval;
}

int Player::getSd() const
{
  return sd;
}

std::string Player::getName() const
{
  return name;
}

int Player::getBalance() const
{
  return balance;
}

bool Player::getFolded() const
{
  return folded;
}

Hand& Player::getHand()
{
  return hand;
}

void Player::setBalance(int b)
{
  balance = b;
}

void Player::setFolded(bool f)
{
  folded = f;
}

void Player::dealHand(Deck* d)
{
  d->dealHand(hand);
}

int Player::getAction() const
{
  return action;
}

void Player::setAction(int a)
{
  action = a;
}

void Player::resetAction()
{
  action = -1;
}

int Player::withdraw(int amount)
{
  if(amount > balance)
  {
    int retval = balance;
    balance = 0;
    return retval;
  }
  else
  {
    balance -= amount;
    return amount;
  }
}

void* Player::threadStarter(void* instance)
{
  return ((Player*)instance)->readSocketInThread(nullptr);
}

/*
 * readSocketInThread
 *
 * @param : fd : int* : the file descriptor to read the socket from
 * @return : void (nullptr)
 *
 * runs in it's own thread, reads requests from the client and
 * changes the client object accordingly
 */
void* Player::readSocketInThread(void* junk)
{
  bool clientStillConnected = true;

  std::string type;
  std::string body;

  while(clientStillConnected)
  {
    if(readMessage(sd, type, body)
    && type.compare("CR") == 0)
    {
      // enqueue the message body
      pthread_mutex_lock(&mutex);
      requestQueue.push_back(body);
      pthread_mutex_unlock(&mutex);

      if(body.compare("exit") == 0)
      {
        clientStillConnected = false;
      }
    }
  }

  return nullptr;
}
