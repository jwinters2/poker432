#pragma once

#include <pthread.h>
#include <vector>

#include "../constants.h"
#include "Global.h"
#include "Player.h"
#include "Deck.h"

class LobbyManager;

enum GamePhase { phase_wait, phase_bet1, phase_swap, phase_bet2, phase_reveal };

class Lobby
{
  static unsigned int maxPlayers;
  static LobbyManager* lm;

  public:
    Lobby(unsigned int, Player*, std::string, std::string);
    ~Lobby();

    void addPlayer(Player*);

    pthread_mutex_t mutex;

    static void setMaxPlayers(unsigned int);
    static void setLobbyManager(LobbyManager*);

    std::string getName() const;
    unsigned int getId() const;
    bool getHasPassword() const;
    unsigned int getPlayerCount();
    unsigned int getVacancy();

    bool checkPassword(std::string) const;

  private:
    unsigned int id;
    std::string lobbyname;
    std::string password;
    Deck* deck;
    GamePhase gamePhase;
    unsigned int potSize;
    unsigned int currentBet;
    int winnerId;

    unsigned int getActiveCount() const;

    static void* threadStarter(void*);
    void* runInThread(void*);

    void handleRequest(unsigned int, Player*, std::string, bool&);
    void sendStatusMessage(int, Player*);

    void nextPhase(GamePhase);

    std::vector<Player*> playerList;
    std::vector<Player*> waitingPlayers;
};
