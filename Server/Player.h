#pragma once

#include <vector>
#include <string>
#include <pthread.h>

#include "../constants.h"
#include "Deck.h"

class Player
{
  static int nextID;

  public:
    Player(int, std::string);
    ~Player();

    bool pollRequest(); // checks if this client has a request we
                        // haven't dealt with yet

    std::string dequeueRequest(); // dequeues the next request to handle

    int getSd() const;
    std::string getName() const;

    pthread_mutex_t mutex;

    int getBalance() const;
    bool getFolded() const;
    Hand& getHand();

    void setBalance(int);
    void setFolded(bool);
    void dealHand(Deck*);

    int withdraw(int);

    int getAction() const;
    void setAction(int);
    void resetAction();

  private:
    std::string name;
    int sd;

    int balance;
    Hand hand;
    bool folded;
    int action;

    static void* threadStarter(void*);
    void* readSocketInThread(void*);

    std::vector<std::string> requestQueue;

    void setName(std::string);
    void enqueueRequest(std::string);
};
