#include "Lobby.h"
#include "LobbyManager.h"
#include "../constants.h"
#include "HandComparer.h"

#include <sys/poll.h>
#include <sstream>
#include <unistd.h>
#include <iostream>

unsigned int  Lobby::maxPlayers = 6;
LobbyManager* Lobby::lm = nullptr;

Lobby::Lobby(unsigned int _id,Player* _player,std::string _n, std::string _p)
            :mutex(PTHREAD_MUTEX_INITIALIZER)
            ,id(_id)
            ,lobbyname(_n)
            ,password(_p)
            ,gamePhase(phase_wait)
            ,potSize(0)
{
  deck = new Deck(1);
  addPlayer(_player);
  pthread_t thread;
  pthread_create(&thread,NULL,threadStarter,this);
}

Lobby::~Lobby()
{
  delete deck;
  pthread_mutex_lock(&mutex);
  for(unsigned int i=0; i<playerList.size(); i++)
  {
    delete playerList[i];
  }
  pthread_mutex_unlock(&mutex);
}

std::string Lobby::getName() const
{
  return lobbyname;
}

bool Lobby::getHasPassword() const
{
  return !password.empty();
}

unsigned int Lobby::getId() const
{
  return id;
}

unsigned int Lobby::getPlayerCount()
{
  pthread_mutex_lock(&mutex);
  unsigned int retval = playerList.size() + waitingPlayers.size();
  pthread_mutex_unlock(&mutex);
  return retval;
}

unsigned int Lobby::getVacancy()
{
  return maxPlayers - getPlayerCount();
}

// this is private and called from within a mutex lock/unlock
// so we don't have to re-lock it (in fact we can't)
unsigned int Lobby::getActiveCount() const
{
  int retval = 0;
  for(unsigned int i=0; i<playerList.size(); i++)
  {
    if(playerList[i] != nullptr && !playerList[i]->getFolded())
    {
      retval++;
    }
  }
  return retval;
}

void Lobby::setMaxPlayers(unsigned int n)
{
  maxPlayers = n;
}

void Lobby::setLobbyManager(LobbyManager* _lm)
{
  lm = _lm;
}

void Lobby::addPlayer(Player* p)
{
  if(p != nullptr)
  {
    pthread_mutex_lock(&mutex);

    waitingPlayers.push_back(p);

    p->setBalance(1000);
    p->setFolded(false);
    p->resetAction();

    sendStatusMessage(-1, p);

    pthread_mutex_unlock(&mutex);
  }
}

bool Lobby::checkPassword(std::string p) const
{
  return (password.empty() || password.compare(p) == 0);
}

void* Lobby::threadStarter(void* instance)
{
  return ((Lobby*)instance)->runInThread(NULL);
}

void* Lobby::runInThread(void* junk)
{
  bool running = true;
  bool sendUpdate;

  while(running)
  {
    sendUpdate = false;

    pthread_mutex_lock(&mutex);

    for(unsigned int i=0; i<playerList.size(); i++)
    {
      if(playerList[i] != nullptr && playerList[i]->pollRequest())
      {
        std::string body = playerList[i]->dequeueRequest();
        pthread_mutex_lock(&coutMutex);
        std::cout << " (L) player " << playerList[i]->getName()
                  << " in lobby " << lobbyname << " requests \"" << body
                  << "\"" << std::endl << std::flush;
        pthread_mutex_unlock(&coutMutex);

        handleRequest(i, playerList[i], body, sendUpdate);
      }
    }

    // if we're in the middle of a game, and all but one person folded or left
    // that person wins the pot and a new round begins
    if(gamePhase != phase_wait && getActiveCount() == 1)
    {
      for(unsigned int i=0; i<playerList.size(); i++)
      {
        if(playerList[i] != nullptr && !playerList[i]->getFolded())
        {
          playerList[i]->setBalance(playerList[i]->getBalance() + potSize);
          potSize = 0;
        }
      }

      nextPhase(phase_wait);
      sendUpdate = true;
    }

    bool changedState;
    do
    {
      changedState = false;

      switch(gamePhase)
      {
        case phase_wait:
        {
          currentBet = 0;
          potSize = 0;
          winnerId = -1;

          // players can leave while waiting
          for(unsigned int i=0; i<playerList.size(); i++)
          {
            if(playerList[i] == nullptr)  
            {
              playerList.erase(playerList.begin() + i);
              i--;
            }
          }

          // add any players currently waiting
          for(unsigned int i=0; i<waitingPlayers.size(); i++)
          {
            playerList.push_back(waitingPlayers[i]);
          }
          waitingPlayers.clear();

          if(playerList.size() >= 2)
          {
            // we're starting the game, reset the deck and deal to everyone
            deck->reset();
            for(unsigned int i=0; i<playerList.size(); i++)
            {
              playerList[i]->dealHand(deck);
              playerList[i]->resetAction();
              playerList[i]->setFolded(false); 
            }

            nextPhase(phase_bet1);
            changedState = true;
            sendUpdate = true;
          }
          else if(!playerList.empty())
          {
            playerList[0]->setBalance(1000);
          }
        }
        break;

        case phase_bet1:
        case phase_bet2:
        {  
          bool doneBetting = true;

          // everyone must have done something
          for(unsigned int i=0; i<playerList.size(); i++)
          {
            if( playerList[i] != nullptr 
            && !playerList[i]->getFolded()
            &&  playerList[i]->getAction() == -1)
            {
              doneBetting = false;
            }
          }

          // everyone must have bet the same amount (or folded)
          for(unsigned int i=0; i<playerList.size(); i++)
          {
            /* a player is "ready" if they're one of the following
             * 1. they folded
             * 2. their bet equals the maximum bet (they don't need to call)
             * 3. they've bet their entire balance
             */
            if(playerList[i] != nullptr
            && (!playerList[i]->getFolded())
            && playerList[i]->getAction() != (int)currentBet
            && playerList[i]->getBalance() > 0)
            {
              doneBetting = false;
            }
          }

          if(doneBetting)
          {
            // clean the currentBet for the next betting phase
            currentBet = 0;
            if(gamePhase == phase_bet1)
            {
              nextPhase(phase_swap);
            }
            else
            {
              nextPhase(phase_reveal);
            }
            changedState = true;
            sendUpdate = true;
          }

        }
        break;

        case phase_swap:
        {
          bool doneSwapping = true;

          for(unsigned int i=0; i<playerList.size(); i++)
          {
            if( playerList[i] != nullptr
            && !playerList[i]->getFolded()
            &&  playerList[i]->getAction() == -1)
            {
              doneSwapping = false;
            }
          }

          if(doneSwapping)
          {
            currentBet = 0;
            nextPhase(phase_bet2);
            changedState = true;
          }
        }
        break;

        case phase_reveal:
        {
          // determine the winner and give them the pot
          winnerId = 0;

          //if the first person folded, skip ahead until we find a non-folded
          // player
          while(playerList[winnerId] == nullptr
             || playerList[winnerId]->getFolded())
          {
            winnerId++;
          }

          // search the rest of the list for a better hand
          for(unsigned int i = winnerId+1; i < playerList.size(); i++)
          {
            if(playerList[i] != nullptr && !playerList[i]->getFolded()
            && compareHands(playerList[i]->getHand(),
                            playerList[winnerId]->getHand()) == 1)
            {
              winnerId = i;
            }
          }

          playerList[winnerId]->setBalance(playerList[winnerId]->getBalance()
                                           + potSize);
          potSize = 0;

          // send everyone the updated game state
          //for(unsigned int i=0; i<playerList.size(); i++)
          //{
            //sendStatusMessage(i, playerList[i]);
          //}

          // kick people out who've run out of money
          for(unsigned int i=0; i<playerList.size(); i++)
          {
            if(playerList[i] != nullptr && playerList[i]->getBalance() == 0)
            {
              if(lm != nullptr)
              {
                pthread_mutex_unlock(&mutex);   // release the mutex for a bit
                lm->addPlayer(playerList[i]);
                pthread_mutex_lock(&mutex);     // to prevent deadlock
              }

              playerList[i] = nullptr;
            }
          }

          bool everyoneReady = true;
          for(unsigned int i=0; i<playerList.size(); i++)
          {
            if(playerList[i] != nullptr && playerList[i]->getAction() == -1)
            {
              everyoneReady = false;
            }
          }

          if(everyoneReady)
          {
            nextPhase(phase_wait);
            changedState = true;
            sendUpdate = true;
          }
        }
        break;
      }
    }
    while(changedState);

    // send an update to everyone if given a valid, game-changing command
    if(sendUpdate)
    {
      for(unsigned int i=0; i<playerList.size(); i++)
      {
        sendStatusMessage(i, playerList[i]);
      }
    }

    running = !playerList.empty();

    pthread_mutex_unlock(&mutex);
  }

  return nullptr;
}

void Lobby::handleRequest(unsigned int pos, Player* player, 
                          std::string body, bool& sendUpdate)
{
  std::stringstream msgStream(body);

  std::string req;

  msgStream >> req;

  if(req.compare("exit") == 0)
  {
    // deletes the player (closing the socket) and removes it from the list
    sendResponse(player->getSd(), "GR", RESPONSE_OK);
    delete player;
    playerList[pos] = nullptr;
    sendUpdate = true;
  }
  else if(req.compare("refresh_game") == 0)
  {
    sendResponse(player->getSd(), "GR", RESPONSE_OK);
    sendStatusMessage(pos, player);
  }
  else if(req.compare("bet") == 0)
  {
    if(gamePhase == phase_bet1 || gamePhase == phase_bet2)
    {
      if(!player->getFolded())
      {
        unsigned int requestedBet;
        unsigned int actualBet;

        if( (msgStream >> requestedBet).fail())
        {
          sendResponse(player->getSd(), "GR", RESPONSE_ERR_BAD_BET);
          return;
        }

        if(player->getAction() != -1)
        {
          requestedBet += currentBet - player->getAction();
        }
        else
        {
          requestedBet += currentBet;
        }

        // Player::withdraw() returns the actual amount bet
        actualBet = player->withdraw(requestedBet);

        // add the money to the pot
        potSize += actualBet;

        // if this is the player's first action (== -1), set it to actualBet
        if(player->getAction() == -1)
        {
          player->setAction(actualBet);
        }
        else
        {
          // otherwise increment it
          player->setAction(player->getAction() + actualBet);
        }

        // keep track of the bet this round
        if(actualBet > currentBet)
        {
          currentBet = actualBet;
        }

        sendResponse(player->getSd(), "GR", RESPONSE_OK);
        sendUpdate = true;
      }
      else
      {
        sendResponse(player->getSd(), "GR", RESPONSE_ERR_ALREADY_FOLDED);
      }
    }
    else
    {
      sendResponse(player->getSd(), "GR", RESPONSE_ERR_WRONG_PHASE);
    }
  }
  else if(req.compare("swap") == 0)
  {
    if(gamePhase == phase_swap)
    {
      unsigned int swapCount;
      unsigned int discard;
      msgStream >> swapCount;

      if(player->getAction() != -1)
      {
        sendResponse(player->getSd(), "GR", RESPONSE_ERR_ALREADY_SWAPPED);
        return;
      }

      if(swapCount < 0 || swapCount > 4)
      {
        sendResponse(player->getSd(), "GR", RESPONSE_ERR_BAD_SWAP_NUM);
        return;
      }

      // we might have duplicate cards (more than one deck)
      // so we need to be careful to not swap the same card twice

      bool swapped[5] = {false, false, false, false, false};
      for(unsigned int i=0; i<swapCount; i++)
      {
        bool cardExists = false;
        msgStream >> discard;
        for(int j=0; j<5; j++)
        {
          if(!swapped[j]
          && cardToInt(player->getHand().card[j]) == (int)discard)
          {
            player->getHand().card[j].set(deck->drawCard());
            cardExists = true;
            swapped[j] = true;
          }
        }

        if(!cardExists)
        {
          sendResponse(player->getSd(), "GR", RESPONSE_ERR_BAD_SWAP_CARD);
          return;
        }
      }

      player->setAction(swapCount);
      sendResponse(player->getSd(), "GR", RESPONSE_OK);
      sendUpdate = true;
    }
    else
    {
      sendResponse(player->getSd(), "GR", RESPONSE_ERR_WRONG_PHASE);
    }
  }
  else if(req.compare("fold") == 0)
  {
    // we can't fold if we're the only one still on the server
    if(getActiveCount() > 1)
    {
      if(gamePhase != phase_reveal && gamePhase != phase_wait)
      {
        player->setFolded(true); 
        sendResponse(player->getSd(), "GR", RESPONSE_OK);
        sendUpdate = true;
      }
      else
      {
        sendResponse(player->getSd(), "GR", RESPONSE_ERR_WRONG_PHASE);
      }
    }
    else
    {
      sendResponse(player->getSd(), "GR", RESPONSE_ERR_GENERIC);
    }
  }
  else if(req.compare("continue") == 0)
  {
    if(gamePhase == phase_reveal)
    {
      player->setAction(0);
      sendResponse(player->getSd(), "GR", RESPONSE_OK);
      sendUpdate = true;
    }
    else
    {
      sendResponse(player->getSd(), "GR", RESPONSE_ERR_WRONG_PHASE);
    }
  }
  else if(req.compare("exit_lobby") == 0)
  {
    sendResponse(player->getSd(), "GR", RESPONSE_OK);
    sendUpdate = true;

    if(lm != nullptr)
    {
      pthread_mutex_unlock(&mutex);   // release the mutex for a bit
      lm->addPlayer(player);
      pthread_mutex_lock(&mutex);     // to prevent deadlock
    }

    playerList[pos] = nullptr;
  }
  else
  {
    sendResponse(player->getSd(), "GR", RESPONSE_ERR_BAD_REQUEST);
  }
}

void Lobby::sendStatusMessage(int id, Player* p)
{
  if(p == nullptr)
  {
    return;
  }

  std::stringstream body;
  body << id << " ";
  for(int i=0; i<5; i++)
  {
    body << cardToInt(p->getHand().card[i]) << " ";
  }

  switch(gamePhase)
  {
    case phase_wait:   body << "wait ";   break;
    case phase_bet1:   body << "bet1 ";   break;
    case phase_swap:   body << "swap ";   break;
    case phase_bet2:   body << "bet2 ";   break;
    case phase_reveal: body << "reveal "; break;
  }

  body << playerList.size() << " ";

  unsigned int activePlayers = 0;
  for(unsigned int i=0; i<playerList.size(); i++)
  {
    if(playerList[i] != nullptr && !playerList[i]->getFolded())
    {
      activePlayers++;
    }
  }

  body << activePlayers << " " << potSize << " ";

  for(unsigned int i=0; i<playerList.size(); i++)
  {
    if(playerList[i] != nullptr && !playerList[i]->getFolded())
    {
      body << i << " " << playerList[i]->getName() << " ";
      body << playerList[i]->getBalance() << " ";
      body << playerList[i]->getAction() << " ";

      // the reveal phase takes slightly different info
      if(gamePhase == phase_reveal)
      {
        for(int j=0; j<5; j++)
        {
          body << cardToInt(playerList[i]->getHand().card[j]) << " ";
        }
        body << ((int)i == winnerId ? "T " : "F ");
      }
    }
  }

  sendMessage(p->getSd(), "GI", body.str());
}

void Lobby::nextPhase(GamePhase p)
{
  // reset everyone's action and change the game phase
  for(unsigned int i=0; i<playerList.size(); i++)
  {
    if(playerList[i] != nullptr)
      playerList[i]->resetAction();
  }
  gamePhase = p;
}
