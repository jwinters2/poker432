#pragma once

#include <pthread.h>

// this is to prevent coutting to be mixed when separate threads try
// to cout at the same time

// this is set in int main()
static pthread_mutex_t coutMutex;
