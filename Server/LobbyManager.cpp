#include <unistd.h>   // read(), write() for file descriptors
#include <cstring>    // memcmp, memcpy
#include <pthread.h>  // pthread_*
#include <sys/poll.h> // pollfd, poll()
#include <iostream>
#include <sstream>

#include <cstdlib>    // rand(), srand()
#include <ctime>      // time() (for srand())

#include "LobbyManager.h"
#include "Global.h"
#include "../constants.h"

LobbyManager::LobbyManager(unsigned int _nl,unsigned int _np,unsigned int _unl)
                          :mutex(PTHREAD_MUTEX_INITIALIZER)
                          ,maxNumLobbies(_nl)
                          ,maxPlayersPerLobby(_np)
                          ,maxUsernameLength(_unl)
{
  // seed the random number generator
  srand(time(NULL));

  // set an array for free lobby ids
  freeIds.assign(maxNumLobbies, true);

  // set static lobby variables
  Lobby::setMaxPlayers(maxPlayersPerLobby);
  Lobby::setLobbyManager(this);

  // start the thread
  pthread_t thread;
  std::cout << "starting lobby manager thread ..." << std::endl;
  pthread_create(&thread, NULL, threadLauncher, this);
}

LobbyManager::~LobbyManager()
{
  pthread_mutex_lock(&mutex);
  for(unsigned int i=0; i<playerList.size(); i++)
  {
    delete playerList[i];
  }
  pthread_mutex_unlock(&mutex);
}

void LobbyManager::addConnection(int fd)
{
  pthread_mutex_lock(&mutex);
  playersConnecting.push_back(fd);
  pthread_mutex_unlock(&mutex);
}

void LobbyManager::addPlayer(Player* p)
{
  if(p != nullptr)
  {
    pthread_mutex_lock(&mutex);
    returnPlayers.push_back(p);
    pthread_mutex_unlock(&mutex);
  }
}

void* LobbyManager::threadLauncher(void* instance)
{
  return ((LobbyManager*)instance)->runInThread(NULL);
}

void* LobbyManager::runInThread(void* param)
{
  std::cout << "lobby manager thread running" << std::endl;
  while(true)
  {
    // first check for players trying to connect
    struct pollfd pfd;
    pthread_mutex_lock(&mutex);
    for(unsigned int i = 0; i < playersConnecting.size(); i++)
    {
      pfd.fd = playersConnecting[i];
      pfd.events = POLLIN;

      // check if the file descriptor has data to read
      if(poll(&pfd, 1, 0) > 0) // the address of the pfd, the count (1)
                               // the timeout (0 ms)
      {
        std::string type;
        std::string body;

        if(readMessage(playersConnecting[i], type, body)
        && type.compare("CR") == 0)
        {
          if(body.substr(0,5).compare("join ") == 0)
          {
            if(body.size() <= maxUsernameLength + 5)
            {
              Player* p = new Player(playersConnecting[i], body.substr(5)); 
              playerList.push_back(p);

              sendResponse(playersConnecting[i], "LR", RESPONSE_OK);
              sendStatusMessage(playersConnecting[i]);

              playersConnecting.erase(playersConnecting.begin() + i);
              i--;
              std::cout << "player " << p->getName() << " joined" << std::endl;
            }
            else
            {
              sendResponse(playersConnecting[i], "LR", 
                           RESPONSE_ERR_NAME_TOO_LONG);
            }
          }
          else
          {
              sendResponse(playersConnecting[i], "LR", 
                           RESPONSE_ERR_UNJOINED);
          }
        }
        else
        {
          std::cout << "malformed request" << std::endl;
          sendResponse(playersConnecting[i], "LR",
                       RESPONSE_ERR_GENERIC);
        }
      }
    }
    pthread_mutex_unlock(&mutex);
    // give addConnection a brief chance to add a new connection

    pthread_mutex_lock(&mutex);

    // next check requests for players already connecting
    for(unsigned int i=0; i<playerList.size(); i++)
    {
      if(playerList[i] != nullptr && playerList[i]->pollRequest())
      {
        pthread_mutex_lock(&coutMutex);
        std::cout << " (LM) dequeueing request \"";
        std::string msgBody = playerList[i]->dequeueRequest();
        std::cout << msgBody << "\"" << std::endl << std::flush;
        pthread_mutex_unlock(&coutMutex);
        
        // handle the request (which sends back a response)
        handleRequest(i, playerList[i], msgBody);
      }
    }

    // remove all null pointers from the array
    for(unsigned int i=0; i<playerList.size(); i++)
    {
      if(playerList[i] == nullptr)
      {
        playerList.erase(playerList.begin() + i);
        i--;
      }
    }
    pthread_mutex_unlock(&mutex);

    pthread_mutex_lock(&mutex);
    // remove all empty lobbies
    for(unsigned int i=0; i<lobbyList.size(); i++)
    {
      if(lobbyList[i] == nullptr || lobbyList[i]->getPlayerCount() == 0)
      {
        if(lobbyList[i] != nullptr)
        {
          delete lobbyList[i];
          freeIds[i] = true;
        }

        lobbyList.erase(lobbyList.begin() + i);
        i--;
      }
    }

    // put players that have left a lobby back into the lobby manager
    for(unsigned int i=0; i<returnPlayers.size(); i++)
    {
      playerList.push_back(returnPlayers[i]);
      sendStatusMessage(playerList.back()->getSd());
    }
    returnPlayers.clear();

    pthread_mutex_unlock(&mutex);
  }
}

void LobbyManager::handleRequest(unsigned int pos, Player* player, 
                                std::string body)
{
  std::stringstream msgStream(body);

  std::string req;

  msgStream >> req;

  if(req.compare("exit") == 0)
  {
    // deletes the player (closing the socket) and removes it from the list
    sendResponse(player->getSd(), "LR", RESPONSE_OK);
    usleep(10000); // 10 msec delay for response to go through
    delete player;
    playerList[pos] = nullptr;
  }
  else if(req.compare("refresh_list") == 0)
  {
    sendResponse(player->getSd(), "LR", RESPONSE_OK);
    usleep(10000);
    sendStatusMessage(player->getSd());
  }
  else if(req.compare("create_lobby") == 0)
  {
    std::string lobbyname;
    std::string password;

    if(!msgStream.good())
    {
      sendResponse(player->getSd(), "LR", RESPONSE_ERR_LOBBY_NO_NAME);
    }
    else
    {
      msgStream >> lobbyname;
      if(msgStream.good())
      {
        msgStream >> password;
      }

      clientCreateLobby(player, lobbyname, password);
    }
  }
  else if(req.compare("enter_lobby") == 0)
  {
    unsigned int id;
    std::string password;

    msgStream >> id;
    if(msgStream.good())
    {
      msgStream >> password;
    }

    clientJoinLobby(player, id, password);
  }
  else if(req.compare("auto_enter") == 0)
  {
    // get a list of valid lobbies (no password, has a vacancy)
    std::vector<Lobby*> openLobbies;
    for(unsigned int i=0; i<lobbyList.size(); i++)
    {
      if(!lobbyList[i]->getHasPassword() && lobbyList[i]->getVacancy() > 0)
      {
        openLobbies.push_back(lobbyList[i]);
      }
    }

    if(openLobbies.empty())
    {
      // there are no open lobbies, create one for them
      clientCreateLobby(player, "Autojoin_Lobby", "");
    }
    else
    {
      unsigned int id = openLobbies[rand() % openLobbies.size()]->getId();
      clientJoinLobby(player, id, "");
    }
  }
  else
  {
    sendResponse(player->getSd(), "LR", RESPONSE_ERR_BAD_REQUEST);
  }
}

void LobbyManager::clientJoinLobby(Player* player, unsigned int id, 
                                   std::string password)
{
  if(id >= 0 && id < lobbyList.size())
  {
    pthread_mutex_unlock(&mutex); // release the mutex for a bit

    // find the position of the lobby with that id
    Lobby* lobbyToJoin = nullptr;
    for(unsigned int i=0; i<lobbyList.size(); i++)
    {
      if(lobbyList[i]->getId() == id)
      {
        lobbyToJoin = lobbyList[i];
        break;
      }
    }

    unsigned int lobbySize = lobbyToJoin->getPlayerCount();
    pthread_mutex_lock(&mutex);   // to prevent deadlock

    if(lobbySize < maxPlayersPerLobby)
    {
      if(lobbyToJoin->checkPassword(password))
      {
        pthread_mutex_unlock(&mutex); // release the mutex for a bit
        lobbyToJoin->addPlayer(player);
        pthread_mutex_lock(&mutex);   // to prevent deadlock

        // the player is in this lobby and therefore no longer in the
        // lobby manager, so remove them
        for(unsigned int i=0; i<playerList.size(); i++)
        {
          if(playerList[i] == player)
          {
            playerList[i] = nullptr;
            break;
          }
        }

        sendResponse(player->getSd(), "LR", RESPONSE_OK);
      }
      else
      {
        sendResponse(player->getSd(), "LR", RESPONSE_ERR_BAD_PASSWORD);
      }
    }
    else
    {
      sendResponse(player->getSd(), "LR", RESPONSE_ERR_LOBBY_FULL);
    }
  }
  else
  {
    sendResponse(player->getSd(), "LR", RESPONSE_ERR_BAD_LOBBY);
  }
}

void LobbyManager::clientCreateLobby(Player* player,
                                     std::string lobbyname,
                                     std::string password)
{
  if(lobbyList.size() + 1 < maxNumLobbies)
  {
    // find the first free Id for the new lobby
    int id = 0;
    while(!freeIds[id])
    {
      id++;
    }

    // create a new lobby (which adds player)
    Lobby* l = new Lobby(id, player, lobbyname, password);
    freeIds[id] = false;

    lobbyList.push_back(l);
    
    // the player is no longer in the lobby manager, so remove them
    for(unsigned int i=0; i<playerList.size(); i++)
    {
      if(playerList[i] == player)
      {
        playerList[i] = nullptr;
        break;
      }
    }

    // send back a response
    sendResponse(player->getSd(), "LR", RESPONSE_OK);
  }
  else
  {
    sendResponse(player->getSd(), "LR", RESPONSE_ERR_LOBBY_LIMIT);
  }
}

void LobbyManager::sendStatusMessage(int fd)
{
  std::string body("");
  body += std::to_string(lobbyList.size()) + " ";
  for(unsigned int i = 0; i<lobbyList.size(); i++)
  {
    body += std::to_string(i) + " ";
    body += lobbyList[i]->getName() + " ";
    body += (lobbyList[i]->getHasPassword() ? "T " : "F " );
    body += std::to_string(lobbyList[i]->getPlayerCount()) + " ";
    body += std::to_string(maxPlayersPerLobby) + " ";
  }

  sendMessage(fd, "LI", body);
}
