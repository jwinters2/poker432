#include "Deck.h"
#include <cstdlib>
#include <ctime>

Deck::Deck(int n):numDecks(n)
{
  reset();
  srand(time(NULL));
}

void Deck::reset()
{
  for(int i=0; i<52; i++)
  {
    cards[i] = numDecks;
  }
  cardCount = 52 * numDecks;
}

int Deck::drawCard()
{
  if(isEmpty())
  {
    return -1;
  }

  int retval;
  unsigned int cardIndex = rand() % cardCount;
  for(int i=0; i<52; i++)
  {
    if(cardIndex < cards[i])
    {
      retval = i;
      break;
    }
    else
    {
      cardIndex -= cards[i]; 
    }
  }

  cards[retval]--;
  cardCount--;
  return retval;
}

bool Deck::dealHand(Hand& h)
{
  if(cardCount < 5)
  {
    return false;
  }

  for(int i=0; i<5; i++)
  h.card[i].set(drawCard());

  return true;
}

bool Deck::isEmpty() const
{
  return cardCount == 0;
}
