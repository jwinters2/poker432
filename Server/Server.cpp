#include <iostream>
#include <pthread.h>
#include <unistd.h>
#include <errno.h>
#include <cstdlib>
#include <strings.h>  // bzero
#include <cstring>    // strerror

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string>

#include "LobbyManager.h"
#include "Global.h"

const int on = 1;

int main(int argc, char** argv)
{
  coutMutex = PTHREAD_MUTEX_INITIALIZER;

  int port = DEFAULT_PORT;
  int maxConnections = 100;
  
  // declare and clean a socket address
  sockaddr_in acceptSocketAddress;
  bzero( (char*)&acceptSocketAddress, sizeof(acceptSocketAddress));

  // setup the socket address
  acceptSocketAddress.sin_family      = AF_INET;
  acceptSocketAddress.sin_addr.s_addr = htonl(INADDR_ANY);
  acceptSocketAddress.sin_port        = htons(port);

  // open the socket
  int serverSd = socket(AF_INET, SOCK_STREAM, 0);

  // something
  setsockopt(serverSd, SOL_SOCKET, SO_REUSEADDR, (char*)&on, sizeof(on));

  // bind socket
  int bindRet = bind(serverSd, (sockaddr*)&acceptSocketAddress,
                     sizeof(acceptSocketAddress));

  if(bindRet == -1)
  {
    std::cerr << "failed to bind socket: " << std::strerror(errno) << std::endl;
    exit(EXIT_FAILURE);
  }

  // listen to the socket
  listen(serverSd, maxConnections);

  // create the lobby manager (which starts its own thread to handle things
  // on its own)
  LobbyManager lm(50,6,32); // max lobbies, max players per lobby
                            // max username length

  while(true)
  {
    sockaddr_in newConnection;
    socklen_t newConnectionSize = sizeof(newConnection);
    
    int fd = accept(serverSd, (sockaddr*)&newConnection, &newConnectionSize);

    // give the new fd to the lobby manager for it to deal with)
    lm.addConnection(fd);
  }
}
