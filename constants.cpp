#include <iostream>
#include <string.h>
#include <cstring>
#include <unistd.h>

#include "constants.h"

Card::Card():rank('0'),suit(NULL_SUIT) {}

Card::Card(char r, Suit s)
{
  set(r,s);
}

Card::Card(int n)
{
  set(n);
}

void Card::set(char r, Suit s)
{
  if(r >= 0 && r < MAX_RANK && s >= 0 && s < NULL_SUIT)
  {
    rank = r;
    suit = s;
  }
}

void Card::set(int n)
{
  if(n >= 0 && n < 52)
  {
    switch(n/4)
    {
      case  8: rank = 'T'; break;
      case  9: rank = 'J'; break;
      case 10: rank = 'Q'; break;
      case 11: rank = 'K'; break;
      case 12: rank = 'A'; break;
      default: rank = (n/4) + '2'; break;
    }
  
    suit = (Suit)(n % 4);
  }
}

std::string Card::toString() const
{
  std::string retval(1,rank); // 1 copy of the character (rank)

  if(rank == 'T')
  {
    retval = "10";
  }

  switch(suit)
  {
    case spades:    retval += "s"; break;
    case clubs:     retval += "c"; break;
    case diamonds:  retval += "d"; break;
    case hearts:    retval += "h"; break;
    case NULL_SUIT: retval += "-"; break;
  }

  return retval;
}

std::string Hand::toString() const
{
  return card[0].toString() + " "
       + card[1].toString() + " "
       + card[2].toString() + " "
       + card[3].toString() + " "
       + card[4].toString();
}

void sendMessage(int fd, const std::string& type, const std::string& body)
{
  if(!body.empty())
  {
    char header[HEADER_SIZE];
    memcpy( header   , "P432", 4); 
    memcpy(&header[4], type.c_str(), 2);
    header[6] = (char)((unsigned short)(body.size()) / 256);
    header[7] = (char)((unsigned short)(body.size()) % 256);

    write(fd, header, HEADER_SIZE);

    unsigned int bytesWritten = 0;
    while(bytesWritten < body.size())
    {
      bytesWritten += write(fd, &(body.c_str())[bytesWritten], 
                            body.size() - bytesWritten);
    }
  }
}

void sendResponse(int fd, const std::string& type, char resp)
{
  char header[HEADER_SIZE + 1];
  memcpy( header   , "P432", 4); 
  memcpy(&header[4], type.c_str(), 2);
  header[6] = 0;
  header[7] = 1;
  header[8] = resp;

  write(fd, header, HEADER_SIZE + 1);
}

bool readMessage(int fd, std::string& type, std::string& body)
{
  char header[HEADER_SIZE];
  read(fd, header, HEADER_SIZE);
  if(memcmp(header,"P432",4) != 0)
  {
    // if there was a problem we still need to clear the rest of the
    // socket
    char junk;
    while(read(fd, &junk, 1) > 0) ;
    return false;
  }

  type.clear();
  type.append(1,header[4]);
  type.append(1,header[5]);

  unsigned short length = (header[6] * 256) + header[7];

  char buffer[length+1];
  buffer[length] = '\0';

  unsigned int bytesRead = 0;
  while(bytesRead < length)
  {
    bytesRead += read(fd, &buffer[bytesRead], length - bytesRead);
  }

  body.assign(buffer);
  return true;
}

char readResponse(int fd, std::string& type)
{
  char header[HEADER_SIZE + 1];
  read(fd, header, HEADER_SIZE + 1);
  if(memcmp(header,"P432",4) != 0)
  {
    return -1;
  }

  type.clear();
  type.append(1,header[4]);
  type.append(1,header[5]);

  return header[8];
}

/*
std::string sha256sum(std::string str)
{
  unsigned char hash[SHA256_DIGEST_LENGTH];
  SHA256_CTX sum;
  SHA256_Init(&sum);
  SHA256_Update(&sum, str.c_str(), str.size());
  SHA256_Final(hash, &sum);

  std::string retval((char*)hash,SHA256_DIGEST_LENGTH);
  return retval;
}
*/

std::string errorToString(char c)
{
  switch(c)
  {
    case RESPONSE_OK:
      return "OK";
    case RESPONSE_ERR_BAD_REQUEST:
      return "Server could not understand request";
    case RESPONSE_ERR_UNJOINED:
      return "You have not joined the server yet";
    case RESPONSE_ERR_NAME_TOO_LONG:
      return "The username or lobby name you entered is too long";
    case RESPONSE_ERR_LOBBY_LIMIT:
      return "Cannot create a lobby: lobby limit reached";
    case RESPONSE_ERR_LOBBY_FULL:
      return "Cannot join lobby: lobby is full";
    case RESPONSE_ERR_BAD_PASSWORD:
      return "Cannot join lobby: incorrect password";
    case RESPONSE_ERR_BAD_LOBBY:
      return "Cannot join lobby: invalid lobby ID";
    case RESPONSE_ERR_WRONG_PHASE:
      return "Cannot perform that action: incorrect game phase";
    case RESPONSE_ERR_BAD_BET:
      return "Cannot bet that amount";
    case RESPONSE_ERR_ALREADY_FOLDED:
      return "Cannot perform that action: you already folded";
    case  RESPONSE_ERR_ALREADY_SWAPPED:
      return "Cannot swap card: you already swapped";
    case  RESPONSE_ERR_BAD_SWAP_NUM:
      return "Cannot swap that number of cards";
    case  RESPONSE_ERR_BAD_SWAP_CARD:
      return "Cannot swap that card: you don't own it";
    case  RESPONSE_ERR_LOBBY_NO_NAME:
      return "Cannot create a lobby: no name given";
    default:
      return "Unknown or Uncaught Error";
  }
}
