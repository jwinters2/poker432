#pragma once
#include <string>

const int DEFAULT_PORT = 3755;
const int HEADER_SIZE = 8;

const int MAX_RANK = 13;

// no error
const char RESPONSE_OK = 0;

// unspecified error
const char RESPONSE_ERR_GENERIC = 1;

// unknown request
const char RESPONSE_ERR_BAD_REQUEST = 2;

// client tried to give a command before "join <username>"
const char RESPONSE_ERR_UNJOINED = 3;      

// join <username> exceeded maximum size
const char RESPONSE_ERR_NAME_TOO_LONG = 4;

// can't create a lobby since the limit has already been reached
const char RESPONSE_ERR_LOBBY_LIMIT = 5;

// can't join a lobby because it doesn't exist
const char RESPONSE_ERR_BAD_LOBBY = 6;

// can't join a lobby because it's full
const char RESPONSE_ERR_LOBBY_FULL = 7;

// can't join a lobby because we gave it a bad password
const char RESPONSE_ERR_BAD_PASSWORD = 8;

// can't do that action because the game is in the wrong phase
// i.e., trying to swap cards when it's time to make a bet
const char RESPONSE_ERR_WRONG_PHASE = 9;

// can't bet that amount (either it's negative, below the current bet
//                        or more than we have)
const char RESPONSE_ERR_BAD_BET = 10;

// can't perform that action because we already folded this round
const char RESPONSE_ERR_ALREADY_FOLDED = 11;

// can't swap cards because we already swapped
const char RESPONSE_ERR_ALREADY_SWAPPED = 12;

// can't swap that number of cards (0 to 3, up to 4 if last card is an ace)
const char RESPONSE_ERR_BAD_SWAP_NUM = 13;

// can't swap that card because we don't own it
const char RESPONSE_ERR_BAD_SWAP_CARD = 14;

// can't create a lobby because no name was given
const char RESPONSE_ERR_LOBBY_NO_NAME = 15;


enum Suit {hearts, clubs, diamonds, spades, NULL_SUIT};

struct Card
{
  Card();
  Card(char, Suit);
  Card(int);

  char rank;
  Suit suit;

  void set(char, Suit);
  void set(int);

  std::string toString() const;
};

struct Hand
{
  Card card[5];

  std::string toString() const;
};

// general utils for sending and receiving messages
void sendMessage(int, const std::string&, const std::string&);
void sendResponse(int, const std::string&, char);

bool readMessage(int, std::string&, std::string&);
char readResponse(int, std::string&);

// hash function (sha-256)
//std::string sha256sum(std::string);

std::string errorToString(char);
