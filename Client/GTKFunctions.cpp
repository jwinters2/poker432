#include "GTKFunctions.h"
#include "../constants.h"
#include <iostream>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/poll.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>
#include <strings.h>

#include <gdk/gdkkeysyms.h>

#include <pthread.h>

int parseUInt(std::string str)
{
  if(str.empty())
  {
    return -1;
  }

  // parses a string as an unsigned int (returns -1 if string is NaN)
  int retval = 0;
  for(unsigned int i=0; i<str.size(); i++)
  {
    if(str[i] >= '0' && str[i] <= '9')
    {
      retval = (10 * retval) + (str[i] - '0');
    }
    else
    {
      return -1;
    }
  }

  return retval;
}

int sendAndGetResponse(int sd, std::string message)
{
  sendMessage(sd, "CR", message);

  int response = -1;

  // another thread is reading all the messages
  while(response == -1)
  {
    pthread_mutex_lock(&mutex);
    response = lastResponse;
    pthread_mutex_unlock(&mutex);
  }

  pthread_mutex_lock(&mutex);
  lastResponse = -1;
  pthread_mutex_unlock(&mutex);

  return response;
}

void PortButtonToggle(GtkWidget* portButton, gpointer data)
{
  // get references to the builder and widgets
  GtkBuilder* builder = (GtkBuilder*)data;

  GtkWidget* label = (GtkWidget*)gtk_builder_get_object(builder, "PortLabel");
  GtkWidget* textbox = (GtkWidget*)gtk_builder_get_object(builder, "PortBox");

  if(gtk_toggle_button_get_active((GtkToggleButton*) portButton))
  {
    // the "default port" button is on, so we disable the custom port stuff
    gtk_widget_set_opacity(label,   0.25);
    gtk_widget_set_opacity(textbox, 0.25);

    // disallow the user to focus the port textbox
    gtk_widget_set_can_focus(textbox, false);

    // and clear any text that was already there
    gtk_entry_set_text((GtkEntry*)textbox, "");
  }
  else
  {
    // the "default port" button is off, so we enable the custom port stuff
    gtk_widget_set_opacity(label,   1.0);
    gtk_widget_set_opacity(textbox, 1.0);

    // allow the user to focus the port textbox
    gtk_widget_set_can_focus(textbox, true);
  }
}

void PasswordButtonToggle(GtkWidget* passwordButton, gpointer data)
{
  // get references to the builder and widgets
  GtkBuilder* builder = (GtkBuilder*)data;

  GtkWidget* label
  = (GtkWidget*)gtk_builder_get_object(builder, "CreateLobbyPasswordLabel");
  GtkWidget* textbox
  = (GtkWidget*)gtk_builder_get_object(builder, "CreateLobbyPasswordBox");

  if(gtk_toggle_button_get_active((GtkToggleButton*) passwordButton))
  {
    gtk_widget_set_opacity(label,   1.0);
    gtk_widget_set_opacity(textbox, 1.0);

    gtk_widget_set_can_focus(textbox, true);
  }
  else
  {
    gtk_widget_set_opacity(label,   0.25);
    gtk_widget_set_opacity(textbox, 0.25);

    gtk_widget_set_can_focus(textbox, false);

    gtk_entry_set_text((GtkEntry*)textbox, "");
  }

}

void ConnectToServer(GtkWidget* connectButton, gpointer data)
{
  // get references to the builder and widgets
  GtkBuilder* builder = (GtkBuilder*)data;

  GtkEntry* hostnameBox
  = (GtkEntry*)gtk_builder_get_object(builder, "HostnameBox");
  GtkEntry* usernameBox
  = (GtkEntry*)gtk_builder_get_object(builder, "UsernameBox");
  GtkToggleButton* portButton
  = (GtkToggleButton*)gtk_builder_get_object(builder, "PortButton");
  GtkEntry* portBox
  = (GtkEntry*)gtk_builder_get_object(builder, "PortBox");

  std::string hostname = gtk_entry_get_text(hostnameBox);
  std::string username = gtk_entry_get_text(usernameBox);

  int port = DEFAULT_PORT; // defined in constants.h
  if(!gtk_toggle_button_get_active(portButton))
  {
    port = parseUInt(gtk_entry_get_text(portBox));
  }

  // these will be error popups in the future
  if(hostname.empty())
  {
    g_print("error, hostname is empty\n");
    return;
  }

  if(username.empty() || username.size() > 32)
  {
    g_print("error, username is empty or too long\n");
    return;
  }

  if(port == -1)
  {
    g_print("error, port unspecified\n");
    return;
  }

  clientSd = getClientSd(hostname, port);
  
  if(clientSd != -1)
  {
    struct argStruct
    {
      GtkBuilder* builder;
      int fd;
    } args;

    args.fd = clientSd;
    args.builder = builder;

    pthread_t thread;
    pthread_create(&thread, NULL, readResponses, &args);

    std::string message = "join " + username;
    int response = sendAndGetResponse(clientSd, message);

    if(response != RESPONSE_OK)
    {
      g_print("error, join failed\n");
      return;
    }

    GtkStack* stack
    = (GtkStack*) gtk_builder_get_object(builder, "MainWindowStack");
    GtkWidget* lobbyviewer
    = (GtkWidget*) gtk_builder_get_object(builder, "LobbyViewer");

    // hide the current child and show the LobbyViewer child
    gtk_widget_hide(gtk_stack_get_visible_child(stack));
    gtk_widget_show(lobbyviewer);
    gtk_stack_set_visible_child(stack, lobbyviewer); 
  }
}

int getClientSd(std::string hostname, int port)
{
  // resolve the server name and store it in host
  struct hostent* host = gethostbyname(hostname.c_str());
  
  if(host == 0)
  {
    return -1;
  }

  // declare and clean a sending socket address
  sockaddr_in sendSocketAddress;
  bzero((char*)&sendSocketAddress, sizeof(sendSocketAddress));

  sendSocketAddress.sin_family      = AF_INET;
  sendSocketAddress.sin_addr.s_addr =
      inet_addr(inet_ntoa(*(struct in_addr*)*host->h_addr_list));
  sendSocketAddress.sin_port        = htons(port);

  int clientSd = socket(AF_INET, SOCK_STREAM, 0);
  if(clientSd == -1)
  {
    return -1;
  }

  if(connect(clientSd, (sockaddr*)&sendSocketAddress,
             sizeof(sendSocketAddress)) != 0)
  {
    return -1;
  }

  return clientSd;
}

void* readResponses(void* param)
{
  struct argStruct
  {
    GtkBuilder* builder;
    int fd;
  };

  GtkBuilder* builder = ((argStruct*)param)->builder;
  int fd = ((argStruct*)param)->fd;
  std::string type;
  std::string body;

  GtkStack* stack
  = (GtkStack*) gtk_builder_get_object(builder, "MainWindowStack");

  while(true)
  {
    readMessage(fd, type, body);

    if(type.compare("LI") == 0)
    {
      pthread_mutex_lock(&lobbyMutex);
      lobbyinfo->parse(body);
      //std::cout << lobbyinfo->toString();
      lobbyinfo->toGtk();
      pthread_mutex_unlock(&lobbyMutex);

      GtkWidget* lobbyviewer
      = (GtkWidget*) gtk_builder_get_object(builder, "LobbyViewer");

      // hide the current child and show the LobbyViewer child
      if(gtk_stack_get_visible_child(stack) != lobbyviewer)
      {
        gtk_widget_hide(gtk_stack_get_visible_child(stack));
        gtk_widget_show(lobbyviewer);
        gtk_stack_set_visible_child(stack, lobbyviewer); 
      }
    }
    else if(type.compare("GI") == 0)
    {
      pthread_mutex_lock(&gameMutex);
      gameinfo->parse(body);
      std::cout << gameinfo->toString();
      pthread_mutex_unlock(&gameMutex);


      GtkWidget* glarea
      = (GtkWidget*) gtk_builder_get_object(builder, "OpenGLArea");

      // hide the current child and show the OpenGLArea child
      if(gtk_stack_get_visible_child(stack) != glarea)
      {
        gtk_widget_hide(gtk_stack_get_visible_child(stack));
        gtk_widget_show(glarea);
        gtk_stack_set_visible_child(stack, glarea); 
      }

      // force the glarea to draw
      gtk_widget_queue_draw(glarea);
    }
    else if(type.compare("LR") == 0 || type.compare("GR") == 0)
    {
      //std::cout << "response (" << type << ") " << errorToString(body[0])
      //          << " (" << (int)body[0] << ")" << std::endl;
      if(body[0] != 0)
      {
        GtkWidget* popup = gtk_message_dialog_new(NULL,
                                                  GTK_DIALOG_MODAL,
                                                  GTK_MESSAGE_ERROR,
                                                  GTK_BUTTONS_CLOSE,
          "ERROR %u\n %s", (int)body[0], errorToString(body[0]).c_str());

        gtk_window_set_title((GtkWindow*) popup, 
          (type.compare("LR") == 0 ? "Lobbying Error" : "Game Error"));

        //g_signal_connect(popup, "close", G_CALLBACK(gtk_window_close), NULL);

        //gtk_dialog_run((GtkDialog*) popup);
        //gtk_widget_destroy(popup);

        g_signal_connect_swapped(popup, "response", 
                                 G_CALLBACK(gtk_widget_destroy), popup);

        gtk_widget_show(popup);
      }

      pthread_mutex_lock(&mutex);
      lastResponse = (int)body[0];
      pthread_mutex_unlock(&mutex);
    }
  }

  return nullptr;
}

void CloseProgram(GtkWidget* button, gpointer data)
{
  if(clientSd != 1)
  {
    sendMessage(clientSd, "CR", "exit");
  }

  delete lobbyinfo;
  delete gameinfo;

  close(clientSd);

  gtk_main_quit();
}

void CreateLobby(GtkWidget* button, gpointer data)
{
  GtkBuilder* builder = (GtkBuilder*)data;

  GtkDialog* popup
  = (GtkDialog*) gtk_builder_get_object(builder, "CreateLobby");
  gtk_window_set_title((GtkWindow*) popup, "Create Lobby");
  gtk_dialog_run(popup);
}

void CreateLobbyConfirm(GtkWidget* button, gpointer data)
{
  GtkBuilder* builder = (GtkBuilder*)data;

  GtkEntry* nameEntry
  = (GtkEntry*) gtk_builder_get_object(builder, "CreateLobbyNameBox");

  GtkEntry* passwordEntry
  = (GtkEntry*) gtk_builder_get_object(builder, "CreateLobbyPasswordBox");

  GtkToggleButton* passwordButton
  = (GtkToggleButton*) gtk_builder_get_object(builder,
    "CreateLobbyPasswordButton");

  std::string name     = gtk_entry_get_text(nameEntry);
  std::string password = gtk_entry_get_text(passwordEntry);

  if(!name.empty())
  {
    if(gtk_toggle_button_get_active(passwordButton))
    {
      if(!password.empty())
      {
        sendMessage(clientSd, "CR", "create_lobby " + name + " " + password);
      }
      else
      {
        g_print("create lobby failed: password\n");
      }
    }
    else
    {
      sendMessage(clientSd, "CR", "create_lobby " + name);
    }
  }
  else
  {
    g_print("create lobby failed: name empty\n");
  }

  // destroy the popup
  GtkWidget* popup
  = (GtkWidget*) gtk_builder_get_object(builder, "CreateLobby");
  gtk_widget_destroy((GtkWidget*) popup);

  // switch to the opengl frame
  // get references
  GtkStack* stack
  = (GtkStack*) gtk_builder_get_object(builder, "MainWindowStack");
  GtkWidget* opengl
  = (GtkWidget*) gtk_builder_get_object(builder, "OpenGLArea");

  // hide the current child and show the LobbyViewer child
  gtk_widget_hide(gtk_stack_get_visible_child(stack));
  gtk_widget_show(opengl);
  gtk_stack_set_visible_child(stack, opengl); 
}

void JoinLobby(GtkWidget* joinButton, gpointer args)
{
  struct argStruct
  {
    GtkBuilder* builder;
    int id;
  };

  GtkBuilder* builder = ((argStruct*)args)->builder;
  int id = ((argStruct*)args)->id;

  if(lobbyinfo->getHasPassword(id))
  {
    GtkDialog* popup
    = (GtkDialog*) gtk_builder_get_object(builder, "GetPassword");
    gtk_window_set_title((GtkWindow*) popup, "Join Lobby");

    // set the "join" button to join this specific lobby
    argStruct ar;
    ar.builder = builder;
    ar.id = id;

    GtkWidget* temp 
    = (GtkWidget*)gtk_builder_get_object(builder, "JoinPasswordConfirm");
    g_signal_connect(temp, "clicked", G_CALLBACK(JoinLobbyPassword), &ar);

    temp = (GtkWidget*) gtk_builder_get_object(builder, "JoinPasswordCancel");
    g_signal_connect(temp, "clicked", G_CALLBACK(gtk_window_close), popup);

    gtk_dialog_run(popup);
  }
  else
  {
    std::string message = "enter_lobby " + std::to_string(id);
    int response = sendAndGetResponse(clientSd, message);

    if(response == RESPONSE_OK)
    {
      // switch to the opengl frame
      // get references
      GtkStack* stack
      = (GtkStack*) gtk_builder_get_object(builder, "MainWindowStack");
      GtkWidget* opengl
      = (GtkWidget*) gtk_builder_get_object(builder, "OpenGLArea");

      // hide the current child and show the LobbyViewer child
      gtk_widget_hide(gtk_stack_get_visible_child(stack));
      gtk_widget_show(opengl);
      gtk_stack_set_visible_child(stack, opengl); 

      // destroy the popup
      GtkWidget* popup
      = (GtkWidget*) gtk_builder_get_object(builder, "GetPassword");
      gtk_widget_hide(popup);
      gtk_widget_destroy(popup);
    }
  }
}

void JoinLobbyPassword(GtkWidget* widget, gpointer args)
{
  struct argStruct
  {
    GtkBuilder* builder;
    int id;
  };

  GtkBuilder* builder = ((argStruct*)args)->builder;
  int id = ((argStruct*)args)->id;

  GtkEntry* passwordBox
  = (GtkEntry*)gtk_builder_get_object(builder, "JoinPasswordBox");

  std::string password = gtk_entry_get_text(passwordBox);

  if(!password.empty())
  {
    std::string message = "enter_lobby " + std::to_string(id)
                        + " " + password; 
    int response = sendAndGetResponse(clientSd, message);

    if(response == RESPONSE_OK)
    {
      // switch to the opengl frame
      // get references
      GtkStack* stack
      = (GtkStack*) gtk_builder_get_object(builder, "MainWindowStack");
      GtkWidget* opengl
      = (GtkWidget*) gtk_builder_get_object(builder, "OpenGLArea");

      // hide the current child and show the LobbyViewer child
      gtk_widget_hide(gtk_stack_get_visible_child(stack));
      gtk_widget_show(opengl);
      gtk_stack_set_visible_child(stack, opengl); 

      // destroy the popup
      GtkWidget* popup
      = (GtkWidget*) gtk_builder_get_object(builder, "GetPassword");
      gtk_widget_hide(popup);
      gtk_widget_destroy(popup);
    }
  }
}

void AutoJoin(GtkWidget* w, gpointer data)
{
  char response = sendAndGetResponse(clientSd, "auto_enter");
  
  if(response == RESPONSE_OK)
  {
    // switch to the opengl frame
    // get references
    GtkStack* stack
    = (GtkStack*) gtk_builder_get_object(builder, "MainWindowStack");
    GtkWidget* opengl
    = (GtkWidget*) gtk_builder_get_object(builder, "OpenGLArea");

    // hide the current child and show the LobbyViewer child
    gtk_widget_hide(gtk_stack_get_visible_child(stack));
    gtk_widget_show(opengl);
    gtk_stack_set_visible_child(stack, opengl); 
  }
}

void RefreshLobbies(GtkWidget* w, gpointer data)
{
  sendAndGetResponse(clientSd, "refresh_list");
}

gboolean KeyPressEvent(GtkWidget* w, GdkEventKey* event, gpointer data)
{
  if(input != nullptr)
  {
    if(event->keyval == GDK_KEY_BackSpace)
    {
      // \0 is interpreted as backspace by the input class
      input->keyPress('\0');
    }
    else if(event->keyval == GDK_KEY_Return)
    {
      input->keyPress('\n');
    }
    else
    {
      input->keyPress((char)event->keyval);
    }
  }
  // force the openglarea to redraw
  GtkBuilder* builder = (GtkBuilder*) data;
  GtkWidget* glarea
  = (GtkWidget*) gtk_builder_get_object(builder, "OpenGLArea");
  gtk_widget_queue_draw(glarea);

  return FALSE;
}

gboolean MouseClickEvent(GtkWidget* w, GdkEventButton* event, gpointer data)
{
  if(input != nullptr)
  {
    input->mouseClick(event->x,event->y);
  }
  
  return FALSE;
}
