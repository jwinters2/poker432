#include "Button.h"
#include "GTKFunctions.h"

#include <gtk/gtk.h>

Button::Button(const Rect& _wr, const Rect& _sr):worldRect(_wr),spriteRect(_sr)
{}

Button::~Button() {}

void Button::onClick() const
{
  // call the function that will be overloaded by the child class
  overloadClick();

  // force opengl to redraw
  GtkWidget* glarea
  = (GtkWidget*) gtk_builder_get_object(builder, "OpenGLArea");
  gtk_widget_queue_draw(glarea);
}

const Rect& Button::getWorldRect() const
{
  return worldRect;
}

const Rect& Button::getSpriteRect() const
{
  return spriteRect;
}

void BetButton::overloadClick() const
{
  if(input->getCurrentBet() >= gameinfo->getMinBet())
  {
    std::string message = "bet " + 
      std::to_string(input->getCurrentBet() - gameinfo->getMinBet());

    sendAndGetResponse(clientSd, message);
  }
}

void FoldButton::overloadClick() const
{
  sendAndGetResponse(clientSd, "fold");
}

void LeaveButton::overloadClick() const
{
  char response = sendAndGetResponse(clientSd, "exit_lobby");
  
  if(response == RESPONSE_OK)
  {
    // switch to the opengl frame
    // get references
    GtkStack* stack
    = (GtkStack*) gtk_builder_get_object(builder, "MainWindowStack");
    GtkWidget* lobbyview
    = (GtkWidget*) gtk_builder_get_object(builder, "LobbyViewer");

    // hide the current child and show the LobbyViewer child
    gtk_widget_hide(gtk_stack_get_visible_child(stack));
    gtk_widget_show(lobbyview);
    gtk_stack_set_visible_child(stack, lobbyview); 
  }
}

void SwapButton::overloadClick() const
{
  int count = 0;
  std::string cards = "";

  for(int i=0; i<5; i++)
  {
    if(input->getSwapCard(i))
    {
      count++;
      cards += std::to_string(gameinfo->getCardValue(i)) + " ";
      input->toggleSwapCard(i);
    }
  }

  std::string message = "swap " + std::to_string(count) + " " + cards;
  sendAndGetResponse(clientSd, message);
}

void ContinueButton::overloadClick() const
{
  sendAndGetResponse(clientSd, "continue");
}

void ToggleCardButton::overloadClick() const
{
  input->toggleSwapCard(cardIndex);
}

void ToggleCardButton::setCardIndex(unsigned int i)
{
  cardIndex = i;
}
