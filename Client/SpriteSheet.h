#pragma once

struct Rect
{
  int x;
  int y;
  int w;
  int h;

  Rect(int,int,int,int);
  Rect(const Rect&);
};

Rect getCardSpr(int);
Rect getCardBackSpr();
Rect getErrCardSpr();
Rect getCharSpr(char);
Rect getProfileSpr(int);
Rect getPlayerBgSpr();
