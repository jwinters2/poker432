#version 330 core

layout(location = 0) in vec2 vertPos;
layout(location = 1) in vec2 vertUV;

out vec2 UV;

void main()
{
  gl_Position.x = (vertPos.x - 335)/335;
  gl_Position.y = (240 - vertPos.y)/240;
  gl_Position.z = 0.0;
  gl_Position.w = 1.0;

  UV.x =      vertUV.x/2048;
  UV.y = 1 - (vertUV.y/2048);
}
