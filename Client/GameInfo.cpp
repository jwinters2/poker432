#include "GameInfo.h"
#include "OpenGL.h"
#include "GTKFunctions.h"

GameInfo::GameInfo()
{
}

GameInfo::~GameInfo()
{
}

int GameInfo::cardToInt(const Card& c) const
{
  if(c.rank == '0' || c.suit == NULL_SUIT)
  {
    return -1;
  }

  int rankValue;
  switch(c.rank)
  {
    case 'T': rankValue = 8; break;
    case 'J': rankValue = 9; break;
    case 'Q': rankValue = 10; break;
    case 'K': rankValue = 11; break;
    case 'A': rankValue = 12; break;
    default : rankValue = (int)(c.rank-'2'); break;
  }

  return (4 * rankValue) + (int)c.suit;
}

void GameInfo::parse(std::string str)
{
  std::stringstream ss(str);

  ss >> userID;
  for(int i=0; i<5; i++)
  {
    int c;
    ss >> c;
    hand.card[i].set(c);
  }
  ss >> gamePhase >> playerCount >> activePlayerCount >> potSize;

  // set the entries size to be the player count, and set everyone to folded
  // (this will get set back in a second)
  entries.resize(activePlayerCount);
  for(int i=0; i<activePlayerCount; i++)
  {
    entries[i].folded = true;
    for(int j=0; j<5; j++)
    {
      entries[i].hand.card[j].set(-1);
    }
  }

  minBet = 0;

  for(int i=0; i<activePlayerCount; i++)
  {
    PlayerEntry pe;

    pe.folded = false;
    ss >> pe.id >> pe.name >> pe.balance >> pe.action;

    // the "winner" is only sent on reveal
    if(checkGamePhase("reveal"))
    {
      char winner = 'F';
      for(int j=0; j<5; j++)
      {
        int c;
        ss >> c;
        pe.hand.card[j].set(c);
      }
      ss >> winner;
      pe.win = (winner == 'T');
    }
    else
    {
      if(checkGamePhase("bet1") || checkGamePhase("bet2"))
      {
        if(pe.action > minBet)
        {
          minBet = pe.action;
        }
      }

      pe.win = false;
    }

    entries[pe.id] = pe;
  }

  // the amount one bets should default to the minimum bet ("calling")
  input->setCurrentBet(minBet);
}

bool GameInfo::checkGamePhase(std::string str) const
{
  return (gamePhase.compare(str) == 0);
}

int GameInfo::getMinBet() const
{
  return minBet;
}

int GameInfo::getCardValue(unsigned int i) const
{
  if(i < 0 || i >= 5)
  {
    return -1;
  }

  return cardToInt(hand.card[i]);
}

std::string GameInfo::toString() const
{
  std::string retval;

  retval += "  UserID : " + std::to_string(userID) + "\n";
  retval += "  Hand : ";
  for(int i=0; i<5; i++)
  {
    retval += hand.card[i].toString() + " (";
    retval += std::to_string(cardToInt(hand.card[i])) + ")   ";
  }

  retval += "\n\n  Phase: " + gamePhase;

  retval += "\n\n  Pot: $" + std::to_string(potSize);

  retval += "\n\n  Players in game : ";
  retval += std::to_string(activePlayerCount) + " / "
          + std::to_string(playerCount) + "\n\n";

  if(checkGamePhase("reveal"))
  {
    retval += "  id  name                              balance  ";
    retval += "hand            \n";
    retval += "  --  --------------------------------  -------  ";
    retval += "----------------\n";
  }
  else
  {
    retval += "  id  name                              balance  action\n";
    retval += "  --  --------------------------------  -------  ------\n";
  }

  for(unsigned int i=0; i<entries.size(); i++)
  {
    retval += (entries[i].id == userID ? "[]" : "  ");
    retval += padString(std::to_string(entries[i].id), 2, false) + "  ";
    retval += padString(entries[i].name, 32 + 2, true);
    retval += padString("$"+std::to_string(entries[i].balance),7,false) + "  ";

    if(checkGamePhase("reveal"))
    {
      for(int j=0; j<5; j++)
      {
        retval += entries[i].hand.card[j].toString() + " ";
      }
    }
    else
    {
      retval += padString(std::to_string(entries[i].action),  6, false) + "  ";
    }

    if(entries[i].win)
    {
      retval += "  WINNER";
    }
    retval += "\n";
  }

  return retval;
}

void GameInfo::draw() const
{
  input->clearButtons();

  for(unsigned int id = 0; id<playerCount; id++)
  {
    // for every player, get the player entry
    if(entries[id].id == userID)
    {
                                                // only show the buttons
                                                // if we haven't folded
      drawPlayer(entries[id], 15, 480 - 125, 1.0f, !entries[id].folded);
    }
    else
    {
      // we're not drawing ourselves with everyone else
      // so drawing everyone where they are creates a gap
      // so draw people "ahead" of us one place early to close the gap
      int pos = (id > userID ? id - 1 : id);

      int x = 10 + 330 * (pos / 3);
      int y = 10 + 75  * (pos % 3);

      drawPlayer(entries[id], x, y, 0.5f, false);
    }
  }

  drawText("Pot: ", 670/2 - 5*15, 480 - 125 - 30, 0.6);
  drawNumber(potSize, 670/2, 480 - 125 - 30, 0.6, true);
}

void GameInfo::drawPlayer(const PlayerEntry& pe, int x, int y, 
                          float s, bool interactive) const
{
  const Hand& h = (interactive ? hand : pe.hand);

  drawSprite(Rect(x, y, 640 * s, 125 * s),getPlayerBgSpr());
  drawSprite(Rect(x + s*10, y + s*10, 60 * s, 60 * s),getProfileSpr(pe.id));

  drawText(pe.name, x + s*10, y + s*86, s*0.6);

  drawNumber(pe.balance, x + s*380, y + s*86, s*0.6, true);

  if(checkGamePhase("bet1") || checkGamePhase("bet2"))
  {
    if(interactive)
    {
      drawNumber(input->getCurrentBet(), x + s*380, y + s*10, s, true);
      int betButtonSprPos = -1;

      if(input->getCurrentBet() == minBet)
      {
        // call button is at (700,800)
        betButtonSprPos = 700;
      }
      else if(input->getCurrentBet() > minBet)
      {
        // raise button is at (800,800)
        betButtonSprPos = 800;
      }

      if(interactive)
      {
        if(betButtonSprPos != -1)
        {
          drawAndAddButton(new BetButton(Rect(x + s*580, y + s*10, s*50, s*25),
                                         Rect(betButtonSprPos,800,100,50)));
        }

      }
    }
    else if(pe.action != -1)
    {
      drawNumber(pe.action, x + s*380, y + s*10, s, true);
    }
    else
    {
      drawText("Hasn't bet yet", x + s*380, y + s*50, s*0.4);
    }
  }
  else if(checkGamePhase("swap"))
  {
    if(interactive)
    {
      for(int i=0; i<5; i++)
      {
        int c = cardToInt(h.card[i]);

        ToggleCardButton* tcb = nullptr; 

        if(input->getSwapCard(i))
        {
          tcb = new ToggleCardButton(
                Rect(x + s*(90 + (60 * i)), y + s*25, s*30, s*45),
                (c != -1 ? getCardSpr(c) : getCardBackSpr()));
        }
        else
        {
          tcb = new ToggleCardButton(
                Rect(x + s*(80 + (60 * i)), y + s*10, s*50, s*75),
                (c != -1 ? getCardSpr(c) : getCardBackSpr()));
        }

        tcb->setCardIndex(i);

        drawAndAddButton(tcb);
      }

      drawAndAddButton(new SwapButton (Rect(x + s*580, y + s*10, s*50, s*25),
                                       Rect(700,950,100,50)));
    }
    else if(pe.action != -1)
    {
      drawText("Swapped ", x + s*380, y + s*40, s*0.4);
      drawNumber(pe.action, x + s*(380 + 10*8), y + s*20, s*0.8, false);
    }
    else
    {
      drawText("Hasn't swapped yet", x + s*380, y + s*50, s*0.4);
    }
  }
  else if(checkGamePhase("reveal"))
  {
    if(pe.win)
    {
      drawText("Winner!", x + s*380, y + s*20, s*0.6);
    }

    if(pe.action != -1)
    {
      drawText("Ready", x + s*380, y + s*50, s*0.4);
    }

    if(interactive)
    {
      drawAndAddButton(new ContinueButton(Rect(x + s*580, y + s*10,
                                          s*50, s*25),
                                          Rect(800,850,100,50)));
    }
  }

  // fold and leave buttons are always present
  if(interactive)
  {
    drawAndAddButton(new FoldButton (Rect(x + s*580, y + s*40, s*50, s*25),
                                     Rect(700,850,100,50)));

    drawAndAddButton(new LeaveButton(Rect(x + s*580, y + s*70, s*50, s*25),
                                     Rect(700,900,100,50)));
  }

  for(int i=0; i<5; i++)
  {
    if(checkGamePhase("wait"))
    {
      // draw the back of cards
      drawSprite(Rect(x + s*(80 + (60 * i)), y + s*10, s*50, s*75),
                      getCardBackSpr());
    }
    else if(!interactive || !checkGamePhase("swap"))
    // in "swap", cards are buttons
    // so we'll deal with that higher up
    {
      // draw the front of cards
      int c = cardToInt(h.card[i]);
      drawSprite(Rect(x + s*(80 + (60 * i)), y + s*10, s*50, s*75),
                     (c != -1 ? getCardSpr(c) : getCardBackSpr()));
    }
  }

  if(pe.folded)
  {
    drawText("Folded", x + s*100, y + s*70, s*0.8);
  }
}

void GameInfo::drawText(std::string text, int x, int y, float s) const
{
  for(unsigned int i=0; i<text.size(); i++)
  {
    drawSprite(Rect(x + s*i*25, y, s*25, s*50), getCharSpr(text[i]));
  }
}

void GameInfo::drawNumber(int num, int x, int y, float s, bool dollar) const
{
  // we'll deal with negative numbers separately, so deal with them here
  int temp = (num >= 0 ? num : -num);
  int size = 0;
  int etcSize = 0;

  // if the number is negative, we need an extra character for the minus sign
  if(num < 0)
  {
    etcSize++;
  }

  // if the number is 0, print 1 digit, otherwise count them
  if(num == 0)
  {
    size = 1;
  }
  else
  {
    while(temp > 0)
    {
      temp /= 10;
      size++;
    }
  }

  // if the number is an amount, we need an extra character for the $
  if(dollar)
  {
    etcSize++;
  }
  
  temp = (num >= 0 ? num : -num);

  for(int i = size-1 + etcSize; i>=etcSize; i--)
  {
    drawSprite(Rect(x + s*i*25, y, s*25, s*50), getCharSpr('0' + (temp%10)));
    temp /= 10;
  }

  if(dollar && num < 0)
  {
    drawSprite(Rect(x, y, s*25, s*50), getCharSpr('$'));
    drawSprite(Rect(x + s*25, y, s*25, s*50), getCharSpr('-'));
  }
  else if(dollar)
  {
    drawSprite(Rect(x, y, s*25, s*50), getCharSpr('$'));
  }
  else if(num < 0)
  {
    drawSprite(Rect(x, y, s*25, s*50), getCharSpr('-'));
  }
}

void GameInfo::drawAndAddButton(Button* b) const
{
  if(b != nullptr && input != nullptr)
  {
    drawSprite(b->getWorldRect(), b->getSpriteRect());
    input->addButton(b);
  }
}

std::string GameInfo::padString(std::string str, 
                                 unsigned int size, bool left) const
{
  std::string retval(str);
  if(str.size() >= size)
  {
    return retval;
  }
  std::string spaces(size - str.size(),' ');

  if(left)
  { 
    retval = str + spaces;
  }
  else
  {
    retval = spaces + str;
  }

  return retval;
}
