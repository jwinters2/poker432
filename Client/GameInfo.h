#pragma once

#include <iostream>
#include <sstream>
#include <vector>

#include "Button.h"
#include "../constants.h"

class GameInfo
{
  struct PlayerEntry
  {
    int id;
    std::string name;
    int balance;
    int action;
    Hand hand;
    bool win;
    bool folded;
  };

  public:
    GameInfo();
    ~GameInfo();

    int userID;
    Hand hand;
    std::string gamePhase;
    int playerCount;
    int activePlayerCount;
    int potSize;
    int minBet;

    bool checkGamePhase(std::string) const;
    int getMinBet() const;

    int getCardValue(unsigned int) const;

    int cardToInt(const Card&) const;
    void parse(std::string);

    void draw() const;

    std::string toString() const;

    std::vector<PlayerEntry> entries;

  private:
    void drawPlayer(const PlayerEntry&, int,int,float,bool) const;
    void drawText(std::string, int, int, float) const;
    void drawNumber(int, int, int, float, bool) const;
    void drawAndAddButton(Button*) const;

    std::string padString(std::string, unsigned int, bool) const;
};
