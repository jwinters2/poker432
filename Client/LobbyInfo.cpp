#include "LobbyInfo.h"
#include "GTKFunctions.h"

#include <gtk/gtk.h>

LobbyInfo::LobbyInfo()
{
}

LobbyInfo::~LobbyInfo()
{
}

void LobbyInfo::parse(std::string str)
{
  entries.clear();
  std::stringstream ss(str);

  int count;
  ss >> count;

  for(int i=0; i<count; i++)
  {
    LobbyEntry le;
    char psswd;
    ss >> le.id >> le.name >> psswd >> le.playerCount >> le.maxPlayers;
    le.hasPassword = (psswd == 'T');
    entries.push_back(le);
  }
}

int LobbyInfo::getVacancy(int id) const
{
  for(unsigned int i=0; i<entries.size(); i++)
  {
    if(entries[i].id == id)
    {
      return entries[i].maxPlayers - entries[i].playerCount;
    }
  }

  return -1;
}

bool LobbyInfo::getHasPassword(int id) const
{
  for(unsigned int i=0; i<entries.size(); i++)
  {
    if(entries[i].id == id)
    {
      return entries[i].hasPassword;
    }
  }

  return false;
}

std::string LobbyInfo::toString() const
{
  if(entries.empty())
  {
    return "No current lobbies, please create one\n";
  }

  std::string retval;
  retval += "  id  name                              mode      players\n";
  retval += "  --  --------------------------------  --------  -------\n";
  for(unsigned int i=0; i<entries.size(); i++)
  {
    std::string id = std::to_string(entries[i].id);
    retval += padString(id, 2 + 2, false) + "  ";

    std::string name(entries[i].name);
    retval += padString(name, 32 + 2, true);

    std::string psswd = (entries[i].hasPassword ? "Private   "
                                                : "          ");
    std::string players = std::to_string(entries[i].playerCount) + " / "
                        + std::to_string(entries[i].maxPlayers);

    retval += psswd;
    retval += padString(players, 7, true);
    retval += "\n";
  }

  return retval;
}

void LobbyInfo::toGtk() const
{
  if(builder == nullptr)
  {
    return;
  }

  // get a reference to the lobbylist
  GtkListBox* lobbylist
  = (GtkListBox*) gtk_builder_get_object(builder, "LobbyList");

  // clear the list
  GList* list = gtk_container_get_children((GtkContainer*) lobbylist);
  for(GList* l = list; l != NULL; l = l->next)
  {
    gtk_widget_destroy((GtkWidget*) l->data);
  }

  for(unsigned int i=0; i<entries.size(); i++)
  {
    GtkWidget* temp;
                              // oritentation and spacing
    GtkBox* box = (GtkBox*) gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 10);

    temp = gtk_label_new(entries[i].name.c_str());
    gtk_widget_set_hexpand(temp,true);
    gtk_container_add((GtkContainer*) box, temp);
    
    std::string playersText = std::to_string(entries[i].playerCount) + " / "
                            + std::to_string(entries[i].maxPlayers);

    gtk_container_add((GtkContainer*) box, gtk_label_new(playersText.c_str()));

    temp = gtk_image_new_from_file("Client/lock_icon.png");
    if(!entries[i].hasPassword)
    {
      gtk_widget_set_opacity(temp, 0.0);
    }
    gtk_container_add((GtkContainer*) box, temp);

    struct
    {
      GtkBuilder* builder;
      int id;
    } args;

    args.builder = builder;
    args.id = entries[i].id;

    temp = gtk_button_new_with_label("Join");
    g_signal_connect(temp, "clicked", G_CALLBACK(JoinLobby), &args);

    gtk_container_add((GtkContainer*) box, temp);

    gtk_widget_show_all((GtkWidget*) box);

                                   // pos of -1 is interpreted as "at the end"
    gtk_list_box_insert(lobbylist, (GtkWidget*) box, -1);
  }
}

std::string LobbyInfo::padString(std::string str, 
                                 unsigned int size, bool left) const
{
  std::string retval(str);
  if(str.size() >= size)
  {
    return retval;
  }
  std::string spaces(size - str.size(),' ');

  if(left)
  { 
    retval = str + spaces;
  }
  else
  {
    retval = spaces + str;
  }

  return retval;
}
