#include <iostream>
#include <errno.h>
#include <cstring>
#include <unistd.h>
#include <cstdlib>
#include <iomanip>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/poll.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>
#include <strings.h>

// for the GUI
#include <gtk/gtk.h>
#include "GTKFunctions.h"
#include "OpenGL.h"

#include "../constants.h"
#include "LobbyInfo.h"
#include "GameInfo.h"
#include "Input.h"

/*
pthread_mutex_t mutex;
bool requestInTransit;

LobbyInfo* lobbyinfo;
pthread_mutex_t lobbyMutex;

GameInfo* gameinfo;
pthread_mutex_t gameMutex;
*/

int lastResponse = -1;
pthread_mutex_t mutex      = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t lobbyMutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t gameMutex  = PTHREAD_MUTEX_INITIALIZER;

LobbyInfo* lobbyinfo = nullptr;
GameInfo*  gameinfo  = nullptr;
Input*     input     = nullptr;

GtkBuilder* builder = nullptr;

int clientSd = -1;

//void* readResponses(void*);

int main(int argc, char** argv)
{
  // this is the Gtk3 part (UI), comment this part out to get the
  // bare-bones, but working cli
  // /* -----------------------------------------------------

  lobbyinfo = new LobbyInfo();
  gameinfo  = new GameInfo();
  input     = new Input();

  gtk_init(&argc, &argv); 

  GtkWidget* window;
  GtkWidget* temp;

  builder = gtk_builder_new();
  gtk_builder_add_from_file(builder, "Client/ClientUI.glade", NULL);

  window = (GtkWidget*)gtk_builder_get_object(builder, "MainWindow");
  g_signal_connect(window, "destroy", G_CALLBACK(CloseProgram), NULL);
  g_signal_connect(window, "key-press-event", 
                   G_CALLBACK(KeyPressEvent), builder);

  temp = (GtkWidget*)gtk_builder_get_object(builder, "PortButton");
  g_signal_connect(temp, "toggled", G_CALLBACK(PortButtonToggle), builder);

  temp = (GtkWidget*)gtk_builder_get_object(builder, "MainConnectButton");
  g_signal_connect(temp, "clicked", G_CALLBACK(ConnectToServer), builder);

  temp = (GtkWidget*)gtk_builder_get_object(builder, "MainQuitButton");
  g_signal_connect(temp, "clicked", G_CALLBACK(CloseProgram), builder);

  temp = (GtkWidget*)gtk_builder_get_object(builder, "QuitButton");
  g_signal_connect(temp, "clicked", G_CALLBACK(CloseProgram), builder);

  temp = (GtkWidget*)gtk_builder_get_object(builder, "CreateLobbyButton");
  g_signal_connect(temp, "clicked", G_CALLBACK(CreateLobby), builder);

  temp = (GtkWidget*)gtk_builder_get_object(builder, "AutojoinButton");
  g_signal_connect(temp, "clicked", G_CALLBACK(AutoJoin), builder);

  temp = (GtkWidget*)gtk_builder_get_object(builder, "RefreshButton");
  g_signal_connect(temp, "clicked", G_CALLBACK(RefreshLobbies), builder);

  temp = (GtkWidget*)gtk_builder_get_object(builder,
    "CreateLobbyPasswordButton");
  g_signal_connect(temp, "toggled", G_CALLBACK(PasswordButtonToggle), builder);

  temp = (GtkWidget*)gtk_builder_get_object(builder, "CreateLobbyConfirm");
  g_signal_connect(temp, "clicked", G_CALLBACK(CreateLobbyConfirm), builder);

  temp = (GtkWidget*)gtk_builder_get_object(builder, "OpenGLArea");
  g_signal_connect(temp, "render", G_CALLBACK(GTKRender), builder);
  g_signal_connect(temp, "realize", G_CALLBACK(GTKRealize), builder);
  gtk_gl_area_set_auto_render((GtkGLArea*)temp, TRUE);

  gtk_widget_add_events(temp, GDK_BUTTON_PRESS_MASK);

  g_signal_connect(temp, "button-press-event", 
                   G_CALLBACK(MouseClickEvent), builder);

  // set a placeholder for the lobbylist (for when it's empty)
  GtkListBox* lobbylist
  = (GtkListBox*)gtk_builder_get_object(builder, "LobbyList");
  GtkWidget* placeholder
  = gtk_label_new("No lobbies available, please create one");
  gtk_widget_show(placeholder);

  gtk_list_box_set_placeholder(lobbylist, placeholder);

  gtk_widget_show_all(window);

  gtk_main();
  return 0;
  // ----------------------------------------------------- */

  //requestInTransit = false;
  mutex      = PTHREAD_MUTEX_INITIALIZER;
  lobbyMutex = PTHREAD_MUTEX_INITIALIZER;
  gameMutex  = PTHREAD_MUTEX_INITIALIZER;

  lobbyinfo = new LobbyInfo();
  gameinfo  = new GameInfo();

  // port and hostname
  int port = 3755;
  char* hostname = argv[1];

  // resolve the server name and store it in host
  struct hostent* host = gethostbyname(hostname);
  
  if(host == 0)
  {
    std::cerr << "Error: could not resolve hostname (" << hostname << ")"
              << std::endl;
    exit(EXIT_FAILURE);
  }

  // declare and clean a sending socket address
  sockaddr_in sendSocketAddress;
  bzero((char*)&sendSocketAddress, sizeof(sendSocketAddress));

  sendSocketAddress.sin_family      = AF_INET;
  sendSocketAddress.sin_addr.s_addr =
      inet_addr(inet_ntoa(*(struct in_addr*)*host->h_addr_list));
  sendSocketAddress.sin_port        = htons(port);

  int clientSd = socket(AF_INET, SOCK_STREAM, 0);
  if(clientSd == -1)
  {
    std::cerr << "Could not open a socket" << std::endl;
    exit(EXIT_FAILURE);
  }

  if(connect(clientSd, (sockaddr*)&sendSocketAddress,
             sizeof(sendSocketAddress)) != 0)
  {
    std::cerr << "Could not connect to server [" << hostname << ":"
              << port << "]" << std::endl;
    exit(EXIT_FAILURE);
  }

  // wait for user to press a space
  std::cout << "connected, enter a username to use" << std::endl << " > ";

  std::string username;
  std::cin >> username;
  std::cin.ignore();
  std::cout << std::endl;
  
  std::string messageBody = "join " + username;

  sendMessage(clientSd, "CR", messageBody);

  char reqBuffer[256];
  std::string request;
  std::string type;

  // print the response for the initial "join" request
  std::cout << "response: " << (int)readResponse(clientSd, type)
            << " (type = " << type << ")" << std::endl;

  pthread_t thread;
  pthread_create(&thread, NULL, readResponses, &clientSd);

  bool reqInT = false;
  while(request.compare("exit") != 0)
  {
    pthread_mutex_lock(&mutex);
    //reqInT = requestInTransit;
    pthread_mutex_unlock(&mutex);

    if(!reqInT)
    {
      std::cin.getline(reqBuffer, 256);
      request.assign(reqBuffer);
      sendMessage(clientSd, "CR", request);

      pthread_mutex_lock(&mutex);
      //requestInTransit = true;
      pthread_mutex_unlock(&mutex);
    }
  }

  close(clientSd);

  delete lobbyinfo;
  delete gameinfo;

  return 0;
}

/*
void* readResponses(void* param)
{
  int fd = *(int*)param;
  std::string type;
  std::string body;

  while(true)
  {
    readMessage(fd, type, body);

    if(type.compare("LI") == 0)
    {
      pthread_mutex_lock(&lobbyMutex);
      lobbyinfo->parse(body);
      std::cout << lobbyinfo->toString();
      pthread_mutex_unlock(&lobbyMutex);
    }
    else if(type.compare("GI") == 0)
    {
      pthread_mutex_lock(&gameMutex);
      gameinfo->parse(body);
      std::cout << gameinfo->toString();
      pthread_mutex_unlock(&gameMutex);
    }
    else if(type.compare("LR") == 0 || type.compare("GR") == 0)
    {
      std::cout << "response (" << type << ") " << errorToString(body[0])
                << " (" << (int)body[0] << ")" << std::endl;
      pthread_mutex_lock(&mutex);
      requestInTransit = false;
      pthread_mutex_unlock(&mutex);
    }
  }

  return nullptr;
}
*/
