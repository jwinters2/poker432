#include "OpenGL.h"
#include "GTKFunctions.h" // for gameinfo

#include <fstream>  // for reading shaders
#include <string>

GLuint Graphics::vertexArrayID = 0;
GLuint Graphics::rectVertexBuffer = 0;
GLuint Graphics::rectUVBuffer = 0;
GLuint Graphics::programID = 0;
GLuint Graphics::spriteSheetID = 0;

gboolean GTKRender(GtkGLArea* area, GdkGLContext* context)
{
  gdk_gl_context_make_current(context);

  // green background, because poker tables tend to be green
  float red   = (float)0x07 / 0x100;
  float green = (float)0x63 / 0x100;
  float blue  = (float)0x24 / 0x100;
  glClearColor(red, green, blue, 1);

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glUseProgram(Graphics::programID);

  GLuint texSamplerID = glGetUniformLocation(Graphics::programID, "texSampler");
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, Graphics::spriteSheetID);
  glUniform1i(texSamplerID, 0);

  gameinfo->draw();

  return TRUE;  
}

void GTKRealize(GtkGLArea* area)
{
  gtk_gl_area_make_current(area);

  glewExperimental = GL_TRUE; // I don't know what this does,
                              // but it segfaults on Bisrat's laptop without it
  glewInit();

  // generate and bind the vertex array object
  glGenVertexArrays(1, &Graphics::vertexArrayID);
  glBindVertexArray(Graphics::vertexArrayID);

  // generate the buffers
  glGenBuffers(1, &Graphics::rectVertexBuffer);
  glGenBuffers(1, &Graphics::rectUVBuffer);

  // load shaders from files
  GLuint vertShaderID = glCreateShader(GL_VERTEX_SHADER);
  GLuint fragShaderID = glCreateShader(GL_FRAGMENT_SHADER);

  std::string vertCode;
  std::string fragCode;
  std::ifstream vertShaderFile("Client/shaders/vertexShader.glsl");
  std::ifstream fragShaderFile("Client/shaders/fragmentShader.glsl");

  if(!vertShaderFile.is_open())
  {
    g_print("Error, vertex shader file could not be opened\n");
    return;
  }

  if(!fragShaderFile.is_open())
  {
    g_print("Error, fragment shader file could not be opened\n");
    return;
  }

  char buffer[256];
  while(vertShaderFile.good())
  {
    vertShaderFile.getline(buffer, 256); 
    vertCode.append(buffer);
    vertCode.append(1,'\n');
  }
  while(fragShaderFile.good())
  {
    fragShaderFile.getline(buffer, 256); 
    fragCode.append(buffer);
    fragCode.append(1,'\n');
  }

  // compile the vertex shader
  char const* vertCodePointer = vertCode.c_str();
  glShaderSource(vertShaderID, 1, &vertCodePointer, NULL);
  glCompileShader(vertShaderID);
  
  // verify that the vertex shader compiled successfully
  GLint result;
  int logSize;
  glGetShaderiv(vertShaderID, GL_COMPILE_STATUS, &result);
  glGetShaderiv(vertShaderID, GL_INFO_LOG_LENGTH, &logSize);
  if(logSize > 0)
  {
    char errorMsg[logSize+1];
    glGetShaderInfoLog(vertShaderID, logSize, NULL, errorMsg);
    g_print(errorMsg);
    g_print("\n");
    return;
  }
  
  // compile the fragment shader
  char const* fragCodePointer = fragCode.c_str();
  glShaderSource(fragShaderID, 1, &fragCodePointer, NULL);
  glCompileShader(fragShaderID);
  
  // verify that the fragment shader compiled successfully
  glGetShaderiv(fragShaderID, GL_COMPILE_STATUS, &result);
  glGetShaderiv(fragShaderID, GL_INFO_LOG_LENGTH, &logSize);
  if(logSize > 0)
  {
    char errorMsg[logSize+1];
    glGetShaderInfoLog(fragShaderID, logSize, NULL, errorMsg);
    g_print(errorMsg);
    g_print("\n");
    return;
  }

  // link them to the program
  Graphics::programID = glCreateProgram();
  glAttachShader(Graphics::programID, vertShaderID);
  glAttachShader(Graphics::programID, fragShaderID);
  glLinkProgram(Graphics::programID);

  // verify that the program linked correctly
  glGetProgramiv(Graphics::programID, GL_LINK_STATUS, &result);
  glGetProgramiv(Graphics::programID, GL_INFO_LOG_LENGTH, &logSize);
  if(logSize > 0)
  {
    char errorMsg[logSize+1];
    glGetShaderInfoLog(fragShaderID, logSize, NULL, errorMsg);
    g_print(errorMsg);
    g_print("\n");
    return;
  }

  // cleanup
  glDetachShader(Graphics::programID, vertShaderID);
  glDetachShader(Graphics::programID, fragShaderID);

  glDeleteShader(vertShaderID);
  glDeleteShader(fragShaderID);

  loadSpriteSheet("Client/spritesheet.bmp");
}

void loadSpriteSheet(std::string filename)
{
  unsigned char header[54]; // all bmp images have a header of 52 bytes
  unsigned int datapos;     // where the actual image starts
  unsigned int width;       // width and height
  unsigned int height;
  unsigned int imageSize;   // size of the image in bytes (w * h * 3)

  std::ifstream file(filename);
  if(!file.is_open())
  {
    g_print("file failed to open: ");
    g_print(filename.c_str());
    g_print("\n");
    return;
  }

  file.read((char*)header, 54);

  if(file.fail() || header[0] != 'B' || header[1] != 'M')
  {
    g_print("file is not valid .bmp file: ");
    g_print(filename.c_str());
    g_print("\n");
    return;
  }

  // parse the metadata
  datapos   = *((int*)(&header[0x0A]));
  imageSize = *((int*)(&header[0x22]));
  width     = *((int*)(&header[0x12]));
  height    = *((int*)(&header[0x16]));

  // not all bmp files specify everything, so get the defaults
  if(datapos == 0)   datapos = 54;
  if(imageSize == 0) imageSize = width * height * 3;

  // we declare this dynamically because file.read neads it for some reason
  unsigned char* data = new unsigned char[imageSize];
  file.seekg(datapos, file.beg);
  file.read((char*)data,imageSize);
  file.close();

  glGenTextures(1, &Graphics::spriteSheetID);
  glBindTexture(GL_TEXTURE_2D, Graphics::spriteSheetID);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, 
               GL_BGR, GL_UNSIGNED_BYTE, data);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, 
                  GL_LINEAR_MIPMAP_LINEAR);

  glGenerateMipmap(GL_TEXTURE_2D);
}

void drawSprite(const Rect& pos, const Rect& uv)
{
  // make arrays for the pos and uv
  GLfloat vertBuf[] =
  {
    (GLfloat)(pos.x)        , (GLfloat)(pos.y + pos.h),
    (GLfloat)(pos.x + pos.w), (GLfloat)(pos.y + pos.h),
    (GLfloat)(pos.x + pos.w), (GLfloat)(pos.y)        ,
    (GLfloat)(pos.x)        , (GLfloat)(pos.y)
  };

  GLfloat uvBuf[] =
  {
    (GLfloat)(uv.x)       , (GLfloat)(uv.y + uv.h),
    (GLfloat)(uv.x + uv.w), (GLfloat)(uv.y + uv.h),
    (GLfloat)(uv.x + uv.w), (GLfloat)(uv.y)       ,
    (GLfloat)(uv.x)       , (GLfloat)(uv.y)
  };

  // bind and set the data for the buffers
  glBindBuffer(GL_ARRAY_BUFFER, Graphics::rectVertexBuffer);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertBuf), vertBuf, GL_STATIC_DRAW);
  glBindBuffer(GL_ARRAY_BUFFER, Graphics::rectUVBuffer);
  glBufferData(GL_ARRAY_BUFFER, sizeof(uvBuf), uvBuf, GL_STATIC_DRAW);

  // draw the vertex buffer
  glEnableVertexAttribArray(0);
  glBindBuffer(GL_ARRAY_BUFFER, Graphics::rectVertexBuffer);
  glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);

  // draw the uv buffer
  glEnableVertexAttribArray(1);
  glBindBuffer(GL_ARRAY_BUFFER, Graphics::rectUVBuffer);
  glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);

  glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

  glDisableVertexAttribArray(0);
  glDisableVertexAttribArray(1);
}
