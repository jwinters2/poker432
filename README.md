Poker432, a CSS432 final project (A networked, client-server poker game)

Protocol documentation: https://docs.google.com/document/d/1AjVJDp67dPqfUcwXiFrikazJ1I06c6Kc5e23eApVQts/edit?usp=sharing

Dependencies:
Server: pthread
Client: pthread, GTK3, OpenGL, GLEW

To compile, run:
$ cmake .
$ make

To run the server:
$ ./server

To run the client:
$ ./client

TODO:
 + More error checking on the server side / more error codes
 + Make a proper UI for the client
 ? Implement a ping system to check for dead connections / abrupt disconnects
 ? Implement a chat system for each lobby

