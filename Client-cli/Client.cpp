#include <iostream>
#include <errno.h>
#include <cstring>
#include <unistd.h>
#include <cstdlib>
#include <iomanip>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/poll.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>
#include <strings.h>

#include <ncurses.h>
#include <fstream>

#include "../constants.h"
#include "LobbyInfo.h"
#include "GameInfo.h"
#include "Input.h"

bool requestInTransit;
bool isInGame;
int lastResponse = -1;

pthread_mutex_t mutex      = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t lobbyMutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t gameMutex  = PTHREAD_MUTEX_INITIALIZER;

WINDOW* mainWindow = nullptr;
WINDOW* inputWindow = nullptr;
WINDOW* responseWindow = nullptr;
std::string inputText = "";

std::string lobbyhelp;
std::string gamehelp;

LobbyInfo* lobbyinfo = nullptr;
GameInfo*  gameinfo  = nullptr;
Input*     input     = nullptr;

int clientSd = -1;

void* readResponses(void*);
void printHelp();

std::string getInputText();

int main(int argc, char** argv)
{
  //requestInTransit = false;
  mutex      = PTHREAD_MUTEX_INITIALIZER;
  lobbyMutex = PTHREAD_MUTEX_INITIALIZER;
  gameMutex  = PTHREAD_MUTEX_INITIALIZER;

  lobbyinfo = new LobbyInfo();
  gameinfo  = new GameInfo();
  isInGame = false;

  if(argc <= 1)
  {
    std::cerr << "you need to type a hostname (serverhost.something.com)"
              << std::endl;
    exit(EXIT_FAILURE);
  }

  // port and hostname
  int port = 3755;
  char* hostname = argv[1];

  // resolve the server name and store it in host
  struct hostent* host = gethostbyname(hostname);
  
  if(host == 0)
  {
    std::cerr << "Error: could not resolve hostname (" << hostname << ")"
              << std::endl;
    exit(EXIT_FAILURE);
  }

  // declare and clean a sending socket address
  sockaddr_in sendSocketAddress;
  bzero((char*)&sendSocketAddress, sizeof(sendSocketAddress));

  sendSocketAddress.sin_family      = AF_INET;
  sendSocketAddress.sin_addr.s_addr =
      inet_addr(inet_ntoa(*(struct in_addr*)*host->h_addr_list));
  sendSocketAddress.sin_port        = htons(port);

  int clientSd = socket(AF_INET, SOCK_STREAM, 0);
  if(clientSd == -1)
  {
    std::cerr << "Could not open a socket" << std::endl;
    exit(EXIT_FAILURE);
  }

  if(connect(clientSd, (sockaddr*)&sendSocketAddress,
             sizeof(sendSocketAddress)) != 0)
  {
    std::cerr << "Could not connect to server [" << hostname << ":"
              << port << "]" << std::endl;
    exit(EXIT_FAILURE);
  }

  // setup ncurses
  initscr();
  cbreak();
  noecho();

  mainWindow     = newwin(22,80,0,0);
  responseWindow = newwin(1,80,22,0);
  inputWindow    = newwin(1,80,23,0);

  // wait for user to press a space
  //std::cout << "connected, enter a username to use" << std::endl << " > ";
  waddstr(mainWindow, "connected, enter a username to use");
  wrefresh(mainWindow);
  wmove(inputWindow, 0,0);
  waddstr(inputWindow, "> ");
  wrefresh(inputWindow);
  
  std::string messageBody = "join " + getInputText();

  sendMessage(clientSd, "CR", messageBody);

  char reqBuffer[256];
  std::string request;
  std::string type;

  // print the response for the initial "join" request
  /*
  wmove(responseWindow,0,2);
  waddstr(responseWindow, "response: ");
  waddstr(responseWindow,
          std::to_string((int)readResponse(clientSd, type)).c_str());
  waddstr(responseWindow, " (type = ");
  waddstr(responseWindow, type.c_str());
  waddstr(responseWindow, ")");
  wrefresh(responseWindow);
  */

  pthread_t thread;
  pthread_create(&thread, NULL, readResponses, &clientSd);

  // load help messages
  char buffer;
  std::ifstream lhfile("Client-cli/lobbyhelp.txt");
  if(lhfile.is_open())
  {
    while(lhfile.good())
    {
      lhfile.get(buffer); 
      lobbyhelp.append(1,buffer);
    }
  }

  std::ifstream ghfile("Client-cli/gamehelp.txt");
  if(ghfile.is_open())
  {
    while(ghfile.good())
    {
      ghfile.get(buffer); 
      gamehelp.append(1,buffer);
    }
  }

  bool reqInT = false;
  while(request.compare("exit") != 0)
  {
    pthread_mutex_lock(&mutex);
    reqInT = requestInTransit;
    pthread_mutex_unlock(&mutex);

    // get a string from the input
    if(!reqInT)
    {
      request.assign(getInputText());

      if(request.compare("help") == 0)
      {
        printHelp();
      }
      else
      {
        sendMessage(clientSd, "CR", request);

        pthread_mutex_lock(&mutex);
        requestInTransit = true;
        pthread_mutex_unlock(&mutex);
      }
    }
  }

  delwin(mainWindow);
  delwin(inputWindow);
  delwin(responseWindow);
  endwin();

  close(clientSd);

  delete lobbyinfo;
  delete gameinfo;

  return 0;
}

void* readResponses(void* param)
{
  int fd = *(int*)param;
  std::string type;
  std::string body;

  while(true)
  {
    readMessage(fd, type, body);

    if(type.compare("LI") == 0)
    {
      pthread_mutex_lock(&lobbyMutex);
      lobbyinfo->parse(body);

      wmove(mainWindow, 0,0);
      wclear(mainWindow);
      waddstr(mainWindow, lobbyinfo->toString().c_str());
      wrefresh(mainWindow);

      wmove(inputWindow, 0,0);     // move to the correct position
      wclrtoeol(inputWindow);
      waddstr(inputWindow, "> ");   // add the prompt
      waddstr(inputWindow, inputText.c_str());  // add the actual text
      wrefresh(inputWindow);      // refresh ncurses

      isInGame = false;
      pthread_mutex_unlock(&lobbyMutex);
    }
    else if(type.compare("GI") == 0)
    {
      pthread_mutex_lock(&gameMutex);
      gameinfo->parse(body);

      wmove(mainWindow, 0,0);
      wclear(mainWindow);
      waddstr(mainWindow, gameinfo->toString().c_str());
      wrefresh(mainWindow);

      wmove(inputWindow, 0,0);     // move to the correct position
      wclrtoeol(inputWindow);
      waddstr(inputWindow, "> ");   // add the prompt
      waddstr(inputWindow, inputText.c_str());  // add the actual text
      wrefresh(inputWindow);      // refresh ncurses

      isInGame = true;
      pthread_mutex_unlock(&gameMutex);
    }
    else if(type.compare("LR") == 0 || type.compare("GR") == 0)
    {
      if((int)body[0] != 0)
      {
        wmove(responseWindow, 0,2);
        wclear(responseWindow);
        waddstr(responseWindow, "Error (");
        waddstr(responseWindow, type.c_str());
        waddstr(responseWindow, "): ");
        waddstr(responseWindow, errorToString(body[0]).c_str());
        waddstr(responseWindow, " (");
        waddstr(responseWindow, std::to_string((int)body[0]).c_str());
        waddstr(responseWindow, ")");
        wrefresh(responseWindow);
        wmove(inputWindow, 0,2);
      }
      pthread_mutex_lock(&mutex);
      requestInTransit = false;
      pthread_mutex_unlock(&mutex);
    }
  }

  return nullptr;
}

void printHelp()
{
  wmove(mainWindow, 0,0);

  if(!isInGame)
  {
    // print lobby help screen
    waddstr(mainWindow, lobbyhelp.c_str());
  }
  else
  {
    // print game help screen
    waddstr(mainWindow, gamehelp.c_str());
  }

  wrefresh(mainWindow);
  wmove(inputWindow, 0,2);
}

std::string getInputText()
{
  wmove(inputWindow, 0,0);     // move to the correct position
  waddstr(inputWindow, "> ");   // add the prompt
  wrefresh(inputWindow);      // refresh ncurses
  
  inputText.clear();
  char c = 0;
  while(inputText.empty() || c != 0x0A)  // return
  {
    c = wgetch(inputWindow);
    if(c == 0x08)   // backspace
    {
      if(inputText.size() > 0)
      {
        inputText.resize(inputText.size() - 1);
      }
    }
    else if(c != 0x0A) // return
    {
      inputText.append(1,c);
    }

    wmove(inputWindow, 0,0);     // move to the correct position
    wclear(inputWindow);
    waddstr(inputWindow, "> ");   // add the prompt
    waddstr(inputWindow, inputText.c_str());  // add the actual text
    wrefresh(inputWindow);      // refresh ncurses
  }

  wmove(inputWindow, 0,0);     // move to the correct position
  wclrtoeol(inputWindow);
  waddstr(inputWindow, "> ");   // add the prompt
  wrefresh(inputWindow);      // refresh ncurses

  std::string retval = inputText;
  inputText.clear();

  return retval;
}
