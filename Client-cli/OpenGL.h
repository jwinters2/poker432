#pragma once

#include <GL/glew.h>
#include <GL/gl.h>
#include <gtk/gtk.h>

#include <string>

#include "SpriteSheet.h"

                               // yes, this is really supposed to be Gdk
                               // instead of Gtk, don't ask me why
gboolean GTKRender(GtkGLArea*, GdkGLContext*);
void GTKRealize(GtkGLArea*);
void loadSpriteSheet(std::string);
void drawSprite(const Rect&, const Rect&);

class Graphics
{
  public:
                  // this is initialized in GTKRealize
    static GLuint programID;
    static GLuint vertexArrayID;
    static GLuint rectVertexBuffer;
    static GLuint rectUVBuffer;
    static GLuint spriteSheetID;
};
