#pragma once

#include <iostream>
#include <sstream>
#include <vector>

#include "../constants.h"

class GameInfo
{
  struct PlayerEntry
  {
    int id;
    std::string name;
    int balance;
    int action;
    Hand hand;
    bool win;
    bool folded;
  };

  public:
    GameInfo();
    ~GameInfo();

    int userID;
    Hand hand;
    std::string gamePhase;
    int playerCount;
    int activePlayerCount;
    int potSize;
    int minBet;

    bool checkGamePhase(std::string) const;
    int getMinBet() const;

    int getCardValue(unsigned int) const;

    int cardToInt(const Card&) const;
    void parse(std::string);

    std::string toString() const;

    std::vector<PlayerEntry> entries;

  private:
    std::string padString(std::string, unsigned int, bool) const;
};
