#pragma once

#include "Button.h"

#include <vector>

class Input
{
  public:
    Input();
    ~Input();

    int getCurrentBet() const;
    void setCurrentBet(int);
    void resetCurrentBet();
    bool getSwapCard(unsigned int) const;
    void toggleSwapCard(unsigned int);

    void keyPress(char);
    void mouseClick(int,int);

    void clearButtons();
    void addButton(Button*);
    
  private:
    int currentBet;
    bool cardsToSwap[5];
    std::vector<Button*> buttons;
};
