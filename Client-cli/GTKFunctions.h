#pragma once

#include <gtk/gtk.h>
#include <string>
#include <pthread.h>

#include "LobbyInfo.h"
#include "GameInfo.h"
#include "Input.h"

extern int clientSd;

extern pthread_mutex_t mutex;
extern int lastResponse;

extern LobbyInfo* lobbyinfo;
extern pthread_mutex_t lobbyMutex;

extern GameInfo* gameinfo;
extern pthread_mutex_t gameMutex;

extern Input* input;

extern GtkBuilder* builder;


int parseUInt(std::string);
int getClientSd(std::string, int);
void* readResponses(void*);
int sendAndGetResponse(int, std::string);

void PortButtonToggle(GtkWidget*, gpointer);
void PasswordButtonToggle(GtkWidget*, gpointer);

void ConnectToServer(GtkWidget*, gpointer);

void CloseProgram(GtkWidget*, gpointer);

void CreateLobby(GtkWidget*, gpointer);
void CreateLobbyConfirm(GtkWidget*, gpointer);

void JoinLobby(GtkWidget*, gpointer);
void JoinLobbyPassword(GtkWidget*, gpointer);

void AutoJoin(GtkWidget*, gpointer);

void RefreshLobbies(GtkWidget*, gpointer);

gboolean KeyPressEvent(GtkWidget*, GdkEventKey*, gpointer);
gboolean MouseClickEvent(GtkWidget*, GdkEventButton*, gpointer);
