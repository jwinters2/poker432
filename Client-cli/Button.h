#pragma once
#include "SpriteSheet.h"

class Button
{
  public:
    Button(const Rect&, const Rect&);
    ~Button();

    void onClick() const;

    const Rect& getWorldRect() const;
    const Rect& getSpriteRect() const;

  protected:
    virtual void overloadClick() const = 0;

  private:
    Rect worldRect;
    Rect spriteRect;
};

class BetButton: public Button
{
  public:
    using Button::Button; // uses parent constructor
    void overloadClick() const;
};

class FoldButton: public Button
{
  public:
    using Button::Button; // uses  parent constructor
    void overloadClick() const;
};

class LeaveButton: public Button
{
  public:
    using Button::Button; // uses  parent constructor
    void overloadClick() const;
};

class SwapButton: public Button
{
  public:
    using Button::Button; // uses  parent constructor
    void overloadClick() const;
};

class ContinueButton: public Button
{
  public:
    using Button::Button; // uses  parent constructor
    void overloadClick() const;
};

class ToggleCardButton: public Button
{
  public:
    using Button::Button; // uses  parent constructor
    void setCardIndex(unsigned int);
    void overloadClick() const;

  private:
    unsigned int cardIndex;
};
