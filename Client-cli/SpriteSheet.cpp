#include "SpriteSheet.h"

Rect::Rect(int _x,int _y,int _w,int _h):x(_x),y(_y),w(_w),h(_h) {}

Rect::Rect(const Rect& o):x(o.x),y(o.y),w(o.w),h(o.h) {}

Rect getCardSpr(int cardNum)
{
             // rows are ranks (n/4), columns are suits (n%4)
             // width is 100, heights are 150
  return Rect(100 * (cardNum / 4), 150 * (cardNum % 4), 100, 150); 
}

Rect getCardBackSpr()
{
  return Rect(1300, 0, 100, 150);
}

Rect getErrCardSpr()
{
  return Rect(1300, 150, 100, 150);
}

Rect getCharSpr(char c)
{
  int w = 25;
  int h = 50;
  return Rect(w * (c%32), 600 + (h * (c/32)), w, h); 
}

Rect getProfileSpr(int n)
{
  return Rect(1400 + (100 * n), 0, 100, 100);
}

Rect getPlayerBgSpr()
{
  return Rect(0, 800, 640, 125);
}
