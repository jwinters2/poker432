#include "Input.h"
#include "GTKFunctions.h"
#include "GameInfo.h"

#include <string>

Input::Input():currentBet(0),cardsToSwap{false,false,false,false,false}
{
}

Input::~Input()
{
}

int Input::getCurrentBet() const
{
  return currentBet;
}

void Input::setCurrentBet(int i)
{
  currentBet = i;
}

void Input::resetCurrentBet()
{
  currentBet = 0;
}

bool Input::getSwapCard(unsigned int i) const
{
  // returns false if i is outside the range [0,5)
  // and doesn't segfault from an array-out-of-bounds
  return (i>=0 && i<5 && cardsToSwap[i]);
}

void Input::toggleSwapCard(unsigned int i)
{
  if(i>=0 && i<5)
  {
    cardsToSwap[i] = !cardsToSwap[i];
  }
}

void Input::clearButtons()
{
  for(unsigned int i=0; i<buttons.size(); i++)
  {
    delete buttons[i];
  }

  buttons.clear();
}

void Input::addButton(Button* b)
{
  buttons.push_back(b);
}

void Input::keyPress(char c)
{
  // 0-9 and backspace are only used for entering bets
  if(c == '\0')
  {
    currentBet /= 10;
  }
  else if(c >= '0' && c <= '9')
  {
    currentBet = (currentBet * 10) + (c - '0');
  }
  else if(c == '\n')
  {
    if(gameinfo->checkGamePhase("bet1") || gameinfo->checkGamePhase("bet2"))
    {
      if(currentBet >= gameinfo->getMinBet())
      {
        std::string message = "bet " + 
          std::to_string(currentBet - gameinfo->getMinBet());

        sendAndGetResponse(clientSd, message);
      }
    }
  }
}

void Input::mouseClick(int x, int y)
{
  // later buttons are on top, so search from top to bottom
  for(int i = buttons.size() - 1; i>=0; i--)
  {
    const Rect& b = buttons[i]->getWorldRect();

    if(x >= b.x && x <= b.x + b.w
    && y >= b.y && y <= b.y + b.h)
    {
      buttons[i]->onClick();
      return;
    }
  }
}

