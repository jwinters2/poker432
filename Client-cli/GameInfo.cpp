#include "GameInfo.h"

GameInfo::GameInfo()
{
}

GameInfo::~GameInfo()
{
}

int GameInfo::cardToInt(const Card& c) const
{
  if(c.rank == '0' || c.suit == NULL_SUIT)
  {
    return -1;
  }

  int rankValue;
  switch(c.rank)
  {
    case 'T': rankValue = 8; break;
    case 'J': rankValue = 9; break;
    case 'Q': rankValue = 10; break;
    case 'K': rankValue = 11; break;
    case 'A': rankValue = 12; break;
    default : rankValue = (int)(c.rank-'2'); break;
  }

  return (4 * rankValue) + (int)c.suit;
}

void GameInfo::parse(std::string str)
{
  std::stringstream ss(str);

  ss >> userID;
  for(int i=0; i<5; i++)
  {
    int c;
    ss >> c;
    hand.card[i].set(c);
  }
  ss >> gamePhase >> playerCount >> activePlayerCount >> potSize;

  // set the entries size to be the player count, and set everyone to folded
  // (this will get set back in a second)
  entries.resize(activePlayerCount);
  for(int i=0; i<activePlayerCount; i++)
  {
    entries[i].folded = true;
    for(int j=0; j<5; j++)
    {
      entries[i].hand.card[j].set(-1);
    }
  }

  minBet = 0;

  for(int i=0; i<activePlayerCount; i++)
  {
    PlayerEntry pe;

    pe.folded = false;
    ss >> pe.id >> pe.name >> pe.balance >> pe.action;

    // the "winner" is only sent on reveal
    if(checkGamePhase("reveal"))
    {
      char winner = 'F';
      for(int j=0; j<5; j++)
      {
        int c;
        ss >> c;
        pe.hand.card[j].set(c);
      }
      ss >> winner;
      pe.win = (winner == 'T');
    }
    else
    {
      if(checkGamePhase("bet1") || checkGamePhase("bet2"))
      {
        if(pe.action > minBet)
        {
          minBet = pe.action;
        }
      }

      pe.win = false;
    }

    entries[pe.id] = pe;
  }
}

bool GameInfo::checkGamePhase(std::string str) const
{
  return (gamePhase.compare(str) == 0);
}

int GameInfo::getMinBet() const
{
  return minBet;
}

int GameInfo::getCardValue(unsigned int i) const
{
  if(i < 0 || i >= 5)
  {
    return -1;
  }

  return cardToInt(hand.card[i]);
}

std::string GameInfo::toString() const
{
  std::string retval;

  retval += "  UserID : " + std::to_string(userID) + "\n";
  retval += "  Hand : ";
  for(int i=0; i<5; i++)
  {
    retval += hand.card[i].toString() + " (";
    retval += std::to_string(cardToInt(hand.card[i])) + ")   ";
  }

  retval += "\n\n  Phase: " + gamePhase;

  retval += "\n\n  Pot: $" + std::to_string(potSize);

  retval += "\n\n  Players in game : ";
  retval += std::to_string(activePlayerCount) + " / "
          + std::to_string(playerCount) + "\n\n";

  if(checkGamePhase("reveal"))
  {
    retval += "  id  name                              balance  ";
    retval += "hand            \n";
    retval += "  --  --------------------------------  -------  ";
    retval += "----------------\n";
  }
  else
  {
    retval += "  id  name                              balance  action\n";
    retval += "  --  --------------------------------  -------  ------\n";
  }

  for(unsigned int i=0; i<entries.size(); i++)
  {
    retval += (entries[i].id == userID ? "[]" : "  ");
    retval += padString(std::to_string(entries[i].id), 2, false) + "  ";
    retval += padString(entries[i].name, 32 + 2, true);
    retval += padString("$"+std::to_string(entries[i].balance),7,false) + "  ";

    if(checkGamePhase("reveal"))
    {
      for(int j=0; j<5; j++)
      {
        retval += entries[i].hand.card[j].toString() + " ";
      }
    }
    else
    {
      retval += padString(std::to_string(entries[i].action),  6, false) + "  ";
    }

    if(entries[i].win)
    {
      retval += "  WINNER";
    }
    retval += "\n";
  }

  return retval;
}

std::string GameInfo::padString(std::string str, 
                                 unsigned int size, bool left) const
{
  std::string retval(str);
  if(str.size() >= size)
  {
    return retval;
  }
  std::string spaces(size - str.size(),' ');

  if(left)
  { 
    retval = str + spaces;
  }
  else
  {
    retval = spaces + str;
  }

  return retval;
}
