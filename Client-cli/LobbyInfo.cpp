#include "LobbyInfo.h"

LobbyInfo::LobbyInfo()
{
}

LobbyInfo::~LobbyInfo()
{
}

void LobbyInfo::parse(std::string str)
{
  entries.clear();
  std::stringstream ss(str);

  int count;
  ss >> count;

  for(int i=0; i<count; i++)
  {
    LobbyEntry le;
    char psswd;
    ss >> le.id >> le.name >> psswd >> le.playerCount >> le.maxPlayers;
    le.hasPassword = (psswd == 'T');
    entries.push_back(le);
  }
}

int LobbyInfo::getVacancy(int id) const
{
  for(unsigned int i=0; i<entries.size(); i++)
  {
    if(entries[i].id == id)
    {
      return entries[i].maxPlayers - entries[i].playerCount;
    }
  }

  return -1;
}

bool LobbyInfo::getHasPassword(int id) const
{
  for(unsigned int i=0; i<entries.size(); i++)
  {
    if(entries[i].id == id)
    {
      return entries[i].hasPassword;
    }
  }

  return false;
}

std::string LobbyInfo::toString() const
{
  if(entries.empty())
  {
    return "No current lobbies, please create one\n";
  }

  std::string retval;
  retval += "  id  name                              mode      players\n";
  retval += "  --  --------------------------------  --------  -------\n";
  for(unsigned int i=0; i<entries.size(); i++)
  {
    std::string id = std::to_string(entries[i].id);
    retval += padString(id, 2 + 2, false) + "  ";

    std::string name(entries[i].name);
    retval += padString(name, 32 + 2, true);

    std::string psswd = (entries[i].hasPassword ? "Private   "
                                                : "          ");
    std::string players = std::to_string(entries[i].playerCount) + " / "
                        + std::to_string(entries[i].maxPlayers);

    retval += psswd;
    retval += padString(players, 7, true);
    retval += "\n";
  }

  return retval;
}

std::string LobbyInfo::padString(std::string str, 
                                 unsigned int size, bool left) const
{
  std::string retval(str);
  if(str.size() >= size)
  {
    return retval;
  }
  std::string spaces(size - str.size(),' ');

  if(left)
  { 
    retval = str + spaces;
  }
  else
  {
    retval = spaces + str;
  }

  return retval;
}
