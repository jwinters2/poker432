#pragma once

#include <iostream>
#include <sstream>
#include <vector>

class LobbyInfo
{
  struct LobbyEntry
  {
    int id;
    std::string name;
    bool hasPassword;
    int playerCount;
    int maxPlayers;
  };

  public:
    LobbyInfo();
    ~LobbyInfo();

    void parse(std::string);

    int getVacancy(int) const;
    bool getHasPassword(int) const;

    std::string toString() const;

  private:
    std::vector<LobbyEntry> entries;

    std::string padString(std::string, unsigned int, bool) const;
};
